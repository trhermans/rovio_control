C++ = g++
#C++ = /Developer/usr/llvm-gcc-4.2/bin/llvm-g++-4.2

C++-OPT = -O3 -DNDEBUG
C++-DEBUG = -g -O0
C++-WARNS = -Wall
#C++-FLAGS = $(C++-WARNS) $(C++-OPT)
C++-FLAGS = $(C++-WARNS) $(C++-DEBUG)
RM = rm -f

PLATFORM =$(shell uname)
ifeq ($(PLATFORM), Darwin)
# Includes
PY_INCLUDE = -I/sw/include/ -I/sw/include/python2.5/
BOOST_INCLUDE = -I/sw/include/boost-1_35/
else
CV_INCLUDE = -I/usr/local/include
PY_INCLUDE = -I/usr/include/ -I/usr/include/python2.6/
BOOST_INCLUDE = -I/usr/include/boost/
endif
INCLUDE = -I./ $(PY_INCLUDE) $(BOOST_INCLUDE) $(CV_INCLUDE) $(VL_INCLUDE) -I./include/ -I./motion/ -I./vision/ -I./planner/ -I./segment/

# Libraries
ifeq ($(PLATFORM), Darwin)
LIB_DIRS = -L./objs/ -L/usr/local/lib/ -L/sw/lib
else
LIB_DIRS = -L./objs/ -L/usr/local/lib/
endif
BOOST_LIBTS = -lboost
ifeq ($(PLATFORM), Darwin)
PY_LIBS = -lpython2.5 -lboost_python
else
PY_LIBS = -lpython2.6 -lboost_python
endif
CURL_LIBS = -lcurlpp -lcurl
CV_LIBS = -lcv -lcvaux -lcxcore -lhighgui
VL_LIBS = -lvl
FREE_IMAGE_LIBS = -lfreeimage
LDLIBS = $(LIB_DIRS) $(PY_LIBS) $(CURL_LIBS) $(CV_LIBS) $(VL_LIBS) $(FREE_IMAGE_LIBS)

# Directory to place all object files
OBJ_DIR = ./build

# Source file listings
PTHREAD_SRCS =  include/PThread.cpp \
		include/PThread.hpp

MOTION_SYSTEM_SRCS = motion/MotionSystem.hpp

VISION_SRCS = vision/Vision.cpp \
	      vision/Vision.hpp

PY_VISION_SRCS = vision/PyVision.cpp \
		 vision/PyVision.hpp

IMAGE_RETRIEVER_SRCS = vision/ImageRetriever.hpp

SEGMENTER_SRCS = vision/Segmenter.hpp

INTERACTIVE_SEGMENTER_SRCS = vision/InteractiveSegmenter.cpp \
		    	     vision/InteractiveSegmenter.hpp

OBJECT_TRACKER_SRCS = vision/ObjectTracker.hpp

SIFT_TRACKER_SRCS = vision/SIFTTracker.cpp \
		    vision/SIFTTracker.hpp

PLANNER_SRCS =  planner/RobotPlanner.cpp \
		planner/RobotPlanner.hpp

PY_MOTION_SRCS = motion/PyMotion.cpp \
		 motion/PyMotion.hpp

ROVIO_MOTION_SRCS = motion/RovioMotionSystem.cpp \
		    motion/RovioMotionSystem.hpp

ROVIO_IMAGE_SRCS = vision/RovioImageRetriever.cpp \
		   vision/RovioImageRetriever.hpp

OFFLINE_IMAGE_SRCS = vision/OfflineImageRetriever.cpp \
		   vision/OfflineImageRetriever.hpp

MOTION_CONTROLLER_SRCS = motion/MotionController.cpp \
			 motion/MotionController.hpp

ROVIO_MESSENGER_SRCS = include/RovioRobotMessenger.cpp \
		       include/RovioRobotMessenger.hpp

# List of object files
OBJS =  $(OBJ_DIR)/PThread.o \
	$(OBJ_DIR)/RobotMessenger.o \
	$(OBJ_DIR)/MotionController.o \
	$(OBJ_DIR)/PyMotion.o \
	$(OBJ_DIR)/RovioMotionSystem.o \
	$(OBJ_DIR)/RovioImageRetriever.o \
	$(OBJ_DIR)/RobotPlanner.o \
	$(OBJ_DIR)/Vision.o \
	$(OBJ_DIR)/SIFTTracker.o \
	$(OBJ_DIR)/InteractiveSegmenter.o \
	$(OBJ_DIR)/PyVision.o

# List of executables
EXECS = rovio-rc offline-vis

# Compilation rules
default: $(EXECS)
remake: clean $(EXECS)
#
# Robot executable
#
rovio-rc: $(OBJ_DIR)/rovio-rc.o
	$(C++) $(C++-FLAGS) $(INCLUDE) $(LDLIBS) $(OBJS) -o $@ $(OBJ_DIR)/rovio-rc.o

$(OBJ_DIR)/rovio-rc.o: rovio-rc.cpp $(OBJS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

test: $(OBJ_DIR)/test.o
	$(C++) $(C++-FLAGS) $(INCLUDE) $(LDLIBS) -o $@ $(OBJ_DIR)/test.o

$(OBJ_DIR)/test.o: test.cpp
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@
#
# Offline executable
#
offline-vis: $(OBJ_DIR)/offline-vis.o $(OBJ_DIR)/OfflineImageRetriever.o
	$(C++) $(C++-FLAGS) $(INCLUDE) $(LDLIBS) $(OBJS) $(OBJ_DIR)/OfflineImageRetriever.o -o $@ $(OBJ_DIR)/offline-vis.o

$(OBJ_DIR)/offline-vis.o: offline-vis.cpp $(OBJS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@
#
# Objects
#
$(OBJ_DIR)/RobotPlanner.o: $(PLANNER_SRCS) $(OBJ_DIR)/PThread.o $(VISION_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/Vision.o: $(VISION_SRCS) $(OBJECT_TRACKER_SRCS) $(SEGMENTER_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/InteractiveSegmenter.o: $(INTERACTIVE_SEGMENTER_SRCS) $(SEGMENTER_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/SIFTTracker.o: $(SIFT_TRACKER_SRCS) $(OBJECT_TRACKER_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/PyMotion.o: $(PY_MOTION_SRCS) $(MOTION_CONTROLLER_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/PyVision.o: $(PY_VISION_SRCS) $(VISION_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/MotionController.o: $(MOTION_CONTROLLER_SRCS) $(MOTION_SYSTEM_SRCS) $(OBJ_DIR)/PThread.o
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/RovioMotionSystem.o: $(ROVIO_MOTION_SRCS) $(MOTION_SYSTEM_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/RovioImageRetriever.o: $(ROVIO_IMAGE_SRCS) $(IMAGE_RETRIEVER_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/OfflineImageRetriever.o: $(OFFLINE_IMAGE_SRCS) $(IMAGE_RETRIEVER_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/RobotMessenger.o: $(ROVIO_MESSENGER_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

$(OBJ_DIR)/PThread.o: $(PTHREAD_SRCS)
	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

# $(OBJ_DIR)/%.o:: %.cpp %.hpp $(OBJS)
# 	$(C++) $(C++-FLAGS) $(INCLUDE) -c $< -o $@

.Phony : clean

clean :
	$(RM) $(EXECS) $(OBJ_DIR)/*.o