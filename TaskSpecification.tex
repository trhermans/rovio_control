\documentclass[11pt]{article}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb} % For set naming (i.e. R^n)

\author{Tucker Hermans}
\title{\textbf{NOTES:} Affordance Learning and Task Specification}

\begin{document}
\maketitle
\begin{enumerate}
\item \textbf{Robot Independent Task Specification} \\
  Here we outline general properties required in specifying a robot independent task.  We are hoping to use this to guide our study of affordance learning, but the task specification is independent of both the robot and the method employed by the robot in solving the task.
  \begin{itemize}
  \item Robot Independent / Robot Agnostic
    \begin{itemize}
    \item Not all robots will be able to complete the task or achieve the goal.
    \item Task does not specify a method only a desired goal.
    \item Inherently scales to use of robot teams instead of single robots.
    \item Specification must be interpretable by the robot (e.g. map for lasers, depth map for stereo vision, etc.)
    \end{itemize}
  \item Nature of specified tasks
    \begin{itemize}
    \item Goals are environment states; not desired positions or activities of the robot.
    \item In general this means tasks will deal more with the environment than specific objects. (This fits well with our interest in solving problems leveraging affordances and classification).
    \end{itemize}
    \item Representation of the end environment and level of user specified data.

      Possible methods of specifying the goal environment to the robot.
      \begin{enumerate}
      \item User specifies a map of the goal environment.
      \item Robot is placed in a correct end environment and collects data, environment is later changed and the robot must recreate the goal environment it has seen.
      \item Robot may decide on the goal environment given some metric (i.e. traversability / path from point \(p_0\) to point \(p_1\)).
      \end{enumerate}
  \end{itemize}

\item \textbf{Entity Specification} \\
  In order for an autonomous robot to learn affordances about entities, the robot must first have some notion of what an entity is within the framework of affordance learning.  Outlined below is the specification we wish to use for a ground robot with limited manipulation abilities. This method is very much dependent on the robot (both in size and sensors used) and an equivalent specification should be designed for any other class of robot used in affordance learning (i.e. humanoid robot, robot arm, etc.).

  \begin{itemize}
  \item Entities are those things on or near the ground plane, which are not walls or other large, static objects.
  \item Near the ground plane means the entity is resting on something on the ground plane, but is still within the workspace of the robot's manipulator (i.e. a book sitting on another book, which is sitting on the ground).
  \item Other stationary objects are things like desks, large bookcases etc, which function as walls in the reference frame of the robot.
  \item We intend to have the robot discover possible entities through use of its laser range finder. The vision system will then be used to build models of the appearance of the entity.
  \item The user must specify a task in generic terms about objects, since objects are not labeled by the robot in any way meaningful to the user.
  \item 
  \end{itemize}

\item \textbf{Affordance Learning for Robot Planning} \\
In order to demonstrate the usefulness of affordance learning in general robotics domains we describe how affordances can be used to help in high level robot planning.  Using the standard planning paradigm of states and actions, affordances can be used to inform the robot as to the outcome (state \(B\)) of performing a given action \(T\) while in a given state \(A\).  For example if a robot is near an object \(O_1\) which has the pushable affordance, then performing the action \textbf{drive} will change the world state such that the position of \(O_1\) is at point \(p_{robot}\).

A bit about terminology.  Here \textbf{affordance} is an absolute idea (i.e. pushable, etc.) and \textbf{affordance value} describes whether a given object has a specified affordance with respect to an actor or other object. As currently formulated the robot has a list of all possible affordances, while it may know some, all, or no affordance values for the objects in its current environment.  Dependent only some subset of all of the affordances known to the robot are pertinent.  It is then important for the robot to learn the affordance values associated with this subset of the affordances in order to create a plan.  We hypothesize that the more affordance values known within the given set of affordances, the better the plan the robot can formulate.

We extend this to a situation where a robot knows all affordance values for the objects in an environment and should thus be able to formulate a good (efficient?, optimal?) plan within the environment. New objects are placed in the environment.  Assume that the task is still achievable without directly interacting with the new objects in the environment, thus the robot can still create an effective plan using the known affordance values.  We believe that the robot could create a better plan if it takes into account the new objects and thus should learn the affordance values of the new objects.

The above explanation is a bit abstract, but it underlines the fact that by using affordances a robot should be able to autonomously build and adapt plans within a changing environment.  This is based on the assumption that a complete list of affordances can be specified at the beginning of the robots use within the environment.  However this method however is better than the two most obvious alternatives: enumerating a list of all possible objects which could be encountered in the environment, or updating the robot's parameters when new objects are inserted into the environment.

States are broken up into \textbf{robot states} and \textbf{world states}.  Robot states are states which characterize the current configuration of the robot.  World states are dependent on the configuration of objects in the environment. As such transitions between world states occur indirectly through robot actions and are independent of most robot states.  The robot does directly control world state variables, such as the position of an object \(O.x\) or a more complicated configuration such as, \(O_1\) is placed on \(O_0\).

\item \textbf{List of Affordances for a Ground Robot} \\
  Here we list a set of affordances to be used with our ground robot.  Additionally we list appropriate actions and states which are associated with the affordances in order accordance with the planning methods discussed above.

  \begin{itemize}
  \item Traversable
    \begin{itemize}
    \item \textbf{Actions:} Drive
    \item \textbf{Robot States:} Driving; Stopped; Stuck
    \end{itemize}
  \item Pushable
    \begin{itemize}
    \item \textbf{Actions:} Push object
    \item \textbf{Robot States:} At object; pushing object
    \end{itemize}
  \item Rollable
    \begin{itemize}
    \item \textbf{Actions:} Short push object
    \item \textbf{Robot States:} At object; Object pushed short
    \end{itemize}
  \item Graspable
    \begin{itemize}
    \item \textbf{Actions:} Close gripper
    \item \textbf{Robot States:} At object; Object Grabbed
    \end{itemize}
  \item Dragable
    \begin{itemize}
    \item \textbf{Actions:} Close Gripper; Drive
    \item \textbf{Robot States:} At Object; Object grabbed; Dragging object
    \end{itemize}
  \item Topplable (can be knocked over)
    \begin{itemize}
    \item \textbf{Actions:} Short push object
    \item \textbf{Robot States:} At object
    \end{itemize}
  \item Liftable
    \begin{itemize}
    \item \textbf{Actions:} Raise gripper
    \item \textbf{Robot States:} Object grasped; Object lifted
    \end{itemize}
  \item Safely Dropable
    \begin{itemize}
    \item \textbf{Actions:} Open gripper
    \item \textbf{Robot States:} Object lifted; Object released; Object dropped
    \end{itemize}
  \end{itemize}

  We believe that affordances are not limited to the paradigm of a joint property between an actor and an object, but may instead be a joint property of two objects of interest to the actor.  We now give some examples of such affordances.

  \begin{itemize}
  \item Stackable (can be stacked on something else)
  \item Supportive (can support something be stacked on it)
  \item Encapsable (can be placed within another object)
  \item Encapsulative (can have objects placed within it)
  \end{itemize}

\item \textbf{Example Tasks} \\
  Here is a list of possible tasks which are achievable by a ground robot using affordance learning.
  \begin{itemize}
  \item The cleanup task. \\
    Move objects in an environment to meet some criteria known as ``clean''. This criteria may be:
    \begin{itemize}
    \item All objects on certain colored ground patches.
    \item All items within \(x\) \texttt{cm} of the wall.
    \item Put each item \(O_i\) within some distance \(\epsilon\) of its desired position \(p_i\).
    \end{itemize}
  \item Traversability / path building in a crowded environment.
    \begin{itemize}
    \item This is essentially the cleanup task with the goal of building a path from point \(A\) to point \(B\).
    \item This may also require determining if unobstructed ground patches are traversable.
    \end{itemize}
  \end{itemize}
\end{enumerate}
\end{document}
