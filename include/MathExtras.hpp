/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef MathExtras_hpp_defined
#define MathExtras_hpp
#include <cmath>

static const float subPIAngle(float theta)
{
    theta = fmod((double)theta, (double)2.0f*M_PI);
    if( theta > M_PI) {
        theta -= 2.0f*M_PI;
    }

    if( theta < -M_PI) {
        theta += 2.0f*M_PI;
    }
    return theta;
}

#endif // MathExtras_hpp_defined
