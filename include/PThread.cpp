/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "PThread.hpp"
using namespace std;

/**
 * Standard constructor for the PThread class
 *
 * @param _name The name of the thread
 */

PThread::PThread(string _name) : name(_name), running(false)
{
    pthread_mutex_init(&myMutex, NULL);
}

PThread::~PThread()
{
    pthread_mutex_destroy(&myMutex);
}

int PThread::start()
{
    if ( !startSubThreads() || running) {
        return -1;
    }

    // Set thread attributes
    pthread_attr_t attr;
    pthread_attr_init (&attr);
    pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);

    // Create thread
    const int result = pthread_create(&thread, &attr, runPThread, (void*)this);

    // Free attribute data
    pthread_attr_destroy(&attr);

    return result;
}

void PThread::stop()
{
    stopSubThreads();
    running = false;
}

void* PThread::runPThread(void* _this)
{
    reinterpret_cast<PThread*>(_this)->run();
    pthread_exit(NULL);
}
void PThread::lock()
{
    pthread_mutex_lock(&myMutex);
}
void PThread::unlock()
{
    pthread_mutex_unlock(&myMutex);
}
