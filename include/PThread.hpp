/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef PThread_hpp_DEFINED
#define PThread_hpp_DEFINED

#include <string>
#include <pthread.h>

/**
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Simple C++ wrapper class of a C pthread
 *
 */
class PThread
{
public:
    // Constructors
    PThread(std::string _name);
    virtual ~PThread();

    // Member Functions
    virtual void lock();
    virtual void unlock();
    virtual int start();
    virtual void stop();

    // Subclasses which spawn child threads should implement these methods
    virtual const bool startSubThreads() { return true; }
    virtual void stopSubThreads() {}

    // Subclass implements this as the code to be run in the thread
    virtual void run() = 0;
    virtual const bool isRunning() const { return running; }
private:
    static void* runPThread(void* _thread);

public:
    const std::string name;

protected:
    bool running;

private:
    pthread_t thread;
    pthread_mutex_t myMutex;
};

#endif // PThread_hpp_DEFINED
