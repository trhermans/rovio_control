/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "RovioRobotMessenger.hpp"

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <iostream>
#include <sstream>

using namespace std;

//
// Constructors
//

/**
 * Standard messenger constructor.
 *
 * @param _robotIP The IP address of the Rovio robot
 */
RovioRobotMessenger::RovioRobotMessenger(string _robotIP) :
    robotIP(_robotIP) {}

//
// Member Functions
//

/**
 * Method to perform the underlying message communication to the Rovio
 *
 * @param _message The HTTP message string to be sent, excluding IP address
 *
 * @return The string received from the robot
 */
string RovioRobotMessenger::sendRobotMessage(string _message)
{
    // cURLpp needs an ostream to write to
    ostringstream dataString(ostringstream::out);

    try {
        // That's all that is needed to do cleanup of used resources
        cURLpp::Cleanup myCleanup;

        // Our request to be sent
        cURLpp::Easy myRequest;

        // Set the request url
        myRequest.setOpt(new cURLpp::Options::Url("http://" +
                                                  robotIP +
                                                  _message));
        // Set where to write the response in memory
        myRequest.setOpt(new cURLpp::Options::WriteStream(&dataString));

        // Send request and get a result.
        myRequest.perform();

    } catch(cURLpp::RuntimeError & e) {
        std::cout << e.what() << std::endl;
    } catch(cURLpp::LogicError & e) {
        std::cout << e.what() << std::endl;
    }
    return dataString.str();
}
