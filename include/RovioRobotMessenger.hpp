/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef RovioRobotMessenger_hpp_DEFINED
#define RovioRobotMessenger_hpp_DEFINED
#include <string>

/**
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief General Messenger Class to deal with communication with the Rovio
 */
class RovioRobotMessenger
{
public:
    // Constructors
    RovioRobotMessenger(std::string _robotIP);
    virtual ~RovioRobotMessenger() {}

    // Member functions
    std::string sendRobotMessage(std::string _message);

private:
    // Members
    std::string robotIP;
};

#endif // RovioRobotMessenger_hpp_DEFINED
