/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @file   TypeDefs.hpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Types to be used across multiple components different files.
 *
 */

#ifndef TypeDefs_hpp_DEFINED
#define TypeDefs_hpp_DEFINED

enum HeadPos {
    HIGH_HEAD = 11,
    LOW_HEAD,
    MID_HEAD
};

struct Estimate2D {
    Estimate2D(float _distance = 0.0f, float _bearing = 0.0f) {
        distance = _distance;
        bearing = _bearing;
    }
    float distance;
    float bearing;
};

struct Estimate3D {
    Estimate3D(float _distance = 0.0f, float _bearing = 0.0f,
               float _elevation = 0.0f){
        distance = _distance;
        bearing = _bearing;
        elevation = _elevation;
    }
    float distance;
    float bearing;
    float elevation;
};

#endif // TypeDefs_hpp_DEFINED
