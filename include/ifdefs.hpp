/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef ifdefs_hpp_DEFINED

//#define RES_320
//#define RES_352
#define RES_640

#ifdef RES_320
#define IMAGE_WIDTH_PIX_DEF 320
#define IMAGE_HEIGHT_PIX_DEF 240
#endif

#ifdef RES_352
#define IMAGE_WIDTH_PIX_DEF 352
#define IMAGE_HEIGHT_PIX_DEF 288
#endif

#ifdef RES_640
#define IMAGE_WIDTH_PIX_DEF 640
#define IMAGE_HEIGHT_PIX_DEF 480
#endif

#endif // ifdefs_hpp_DEFINED
