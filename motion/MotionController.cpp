/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @file   MotionController.cpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Houses the implementation of the MotionController class
 *
 */

#include "MotionController.hpp"
#include <iostream>
#include <cmath> // for fabs
#include <Timer.hpp>
using namespace std;

//
// Constructors
//

MotionController::MotionController(boost::shared_ptr<MotionSystem> _system) :
    PThread("Motion"), system(_system), changed(false) {}

//
// Member Functions
//

/**
 * Method called when the thread begins running
 */
void MotionController::run()
{
    // Clock rate for sending drive commands
    const long long FRAME_LENGTH = 500000;
    // Signal thread start
    running = true;
    unsigned int frameCounter = 0;
    cout << "Running motion controller" << endl;
    while (running) {
        // Record our start time
        const long long startTime = micro_time();

        lock();
        runStep();
        unlock();
        system->runStep();
        const long long sleepTime = FRAME_LENGTH - (micro_time() - startTime);
        // Wait before we run again
        if (sleepTime > 0) {
            usleep(static_cast<useconds_t>(sleepTime));
        }
    }
    running = false;
}

void MotionController::runStep()
{
    // Determine motion command to run
    if (changed) {
        // Perform motion command
        system->setMoveCommand(xCmd, yCmd, thetaCmd);
        if (fabs(thetaCmd) == 20.0f) {
            thetaCmd = 0;
        }
        changed = false;
    }
    // Change Head Position
    if (raiseSpeed > 0) {
        system->raiseHead(raiseSpeed);
        raiseSpeed = 0;
    }
    if (lowerSpeed > 0) {
        system->lowerHead(lowerSpeed);
        lowerSpeed = 0;
    }
}
