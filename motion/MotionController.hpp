/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief Class to control the robot motion
 *
 */

#ifndef MotionController_hpp_DEFINED
#define MotionController_hpp_DEFINED
#include <boost/shared_ptr.hpp>
#include "MotionSystem.hpp"
#include "PThread.hpp"

class MotionController : public PThread
{
public:
    // Constructors
    MotionController(boost::shared_ptr<MotionSystem> _system);
    virtual ~MotionController() {}

    // Member Functions
    virtual void run();
    void runStep();
    void raiseHead() {
        raiseSpeed = 5;
    }
    void lowerHead() {
        lowerSpeed = 5;
    }
    void setSpeed(float _x, float _y, float _theta) {
        xCmd = _x;
        yCmd = _y;
        thetaCmd = _theta;
        changed = true;
    }

    void setHeadVal (HeadPos _pos) { system->setHeadVal(_pos); }
    const HeadPos getHeadVal() const { return system->getHeadVal(); }
    boost::shared_ptr<MotionSystem> system;

private:
    // Members
    bool changed;
    float xCmd;
    float yCmd;
    float thetaCmd;
    int raiseSpeed;
    int lowerSpeed;
};

#endif // MotionController_hpp_DEFINED
