/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef MotionSystem_hpp_DEFINED
#define MotionSystem_hpp_DEFINED

#include "TypeDefs.hpp"

class MotionSystem
{
public:
    virtual void runStep(void) = 0;
    virtual void setMoveCommand(float x, float y, float theta) = 0;
    virtual void raiseHead(int speed) = 0;
    virtual void lowerHead(int speed) = 0;
    virtual void setHeadVal(HeadPos _pos) = 0;
    virtual const HeadPos getHeadVal() const = 0;
};
#endif // MotionSystem_hpp_DEFINED
