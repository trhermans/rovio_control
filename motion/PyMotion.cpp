/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include <boost/python.hpp>
#include <boost/python/object.hpp>
#include <boost/python/module.hpp>
#include <boost/python/args.hpp>
#include <boost/shared_ptr.hpp>
using namespace std;
using namespace boost::python;
using namespace boost;

#include "MotionController.hpp"

static shared_ptr<MotionController> motion_reference;

HeadPos identity_(HeadPos headVal) { return headVal; }

/**
 * Class to hold the motion interfacing needed for python
 *
 */
class PyMotion {
private:
    shared_ptr<MotionController> motion;
public:
    PyMotion() {
        motion = motion_reference;
    }
    void setSpeed(float _x, float _y, float _theta) {
        motion->lock();
        motion->setSpeed(_x, _y, _theta);
        motion->unlock();
    }
    void stopMoving() {
        motion->lock();
        motion->setSpeed(0,0,0);
        motion->unlock();
    }
    void raiseHead() {
        motion->lock();
        motion->raiseHead();
        motion->unlock();
    }
    void lowerHead() {
        motion->lock();
        motion->lowerHead();
        motion->unlock();
    }
    void setHeadVal(HeadPos headVal) {
        motion->lock();
        motion->setHeadVal(headVal);
        motion->unlock();
    }
    HeadPos getHeadVal() {
        HeadPos val;
        motion->lock();
        val = motion->getHeadVal();
        motion->unlock();
        return val;
    }

    void spin20DegPos() {
        motion->lock();
        motion->setSpeed(0, 0, 20.0);
        motion->unlock();
    }

    void spin20DegNeg() {
        motion->lock();
        motion->setSpeed(0, 0, -20.0);
        motion->unlock();
    }
};

BOOST_PYTHON_MODULE(_motion)
{
    enum_<HeadPos>("HeadPos")
        .value("HIGH_HEAD", HIGH_HEAD)
        .value("LOW_HEAD", LOW_HEAD)
        .value("MID_HEAD", MID_HEAD)
        .export_values()
        ;
    def("identity", identity_);

    class_<PyMotion>("Motion")
        // functional
        .def("setSpeed", &PyMotion::setSpeed, "Set the robot's speed")
        .def("stopMoving", &PyMotion::stopMoving, "Stop the robot from driving")
        .def("raiseHead", &PyMotion::raiseHead, "Raise the robot's head")
        .def("lowerHead", &PyMotion::lowerHead, "Lower the robot's head")
        .def("spin20DegPos", &PyMotion::spin20DegPos,
             "Spin 20 degrees in the positive direction")
        .def("spin20DegNeg", &PyMotion::spin20DegNeg,
             "Spin 20 degrees in the negative direction")
        .add_property("headPosition", &PyMotion::getHeadVal,
                      &PyMotion::setHeadVal)
        ;
}

void c_init_motion() {
    if (!Py_IsInitialized())
        Py_Initialize();
    try {
        init_motion();
    } catch (error_already_set) {
        PyErr_Print();
    }
}

void set_motion_reference(shared_ptr<MotionController> _motion)
{
    motion_reference = _motion;
}
