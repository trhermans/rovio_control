/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "RovioMotionSystem.hpp"
#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

const string RovioMotionSystem::NAV_ACTION_PREFIX = "/rev.cgi?Cmd=nav&action=";
const string RovioMotionSystem::DRIVE_ACTION_PREFIX = "&drive=";
const string RovioMotionSystem::SPEED_ACTION_PREFIX = "&speed=";
const string RovioMotionSystem::DRIVE_ACTION_VALUE = "18";
const string RovioMotionSystem::STOP_VALUE = "0";
const string RovioMotionSystem::POS_X_VALUE = "1";
const string RovioMotionSystem::NEG_X_VALUE = "2";
const string RovioMotionSystem::POS_Y_VALUE = "3";
const string RovioMotionSystem::NEG_Y_VALUE = "4";
const string RovioMotionSystem::POS_THETA_VALUE = "5";
const string RovioMotionSystem::NEG_THETA_VALUE = "6";
const string RovioMotionSystem::POS_X_POS_Y_VALUE = "7";
const string RovioMotionSystem::POS_X_NEG_Y_VALUE = "8";
const string RovioMotionSystem::NEG_X_POS_Y_VALUE = "9";
const string RovioMotionSystem::NEG_X_NEG_Y_VALUE = "10";
const string RovioMotionSystem::HEAD_UP_VALUE = "11";
const string RovioMotionSystem::HEAD_DOWN_VALUE = "12";
const string RovioMotionSystem::HEAD_MID_VALUE = "13";
const string RovioMotionSystem::POS_20_THETA_VALUE = "17";
const string RovioMotionSystem::NEG_20_THETA_VALUE = "18";

RovioMotionSystem::RovioMotionSystem(string _robotIP) :
    RovioRobotMessenger(_robotIP), headPos(HIGH_HEAD), x(0.0f), y(0.0f),
    theta(0.0f) {}

/**
 * Main control loop which is called at every frame step
 */
void RovioMotionSystem::runStep()
{
    if (fabs(theta) == 20.0f) {
        if (theta > 0) {
            sendDriveCommand(POS_20_THETA_VALUE, 10);
        } else {
            sendDriveCommand(NEG_20_THETA_VALUE, 10);
        }
        theta = 0.0f;
        return;
    }

    //
    // Drive Commands
    // Convert setSpeed to the Rovio's abilities
    //
    if (x > 0) {
        if (y > 0) {
            sendDriveCommand(POS_X_POS_Y_VALUE, x);
        } else if (y < 0) {
            sendDriveCommand(POS_X_NEG_Y_VALUE, x);
        } else {
            sendDriveCommand(POS_X_VALUE, x);
        }
    } else if (x < 0) {
        if (y > 0) {
            sendDriveCommand(NEG_X_POS_Y_VALUE, x);
        } else if (y < 0) {
            sendDriveCommand(NEG_X_NEG_Y_VALUE, x);
        } else {
            sendDriveCommand(NEG_X_VALUE, x);
        }
    } else if (y > 0) { // LATERAL MOVEMENT
        sendDriveCommand(POS_Y_VALUE, y);
    } else if (y < 0) {
        sendDriveCommand(NEG_Y_VALUE, y);
    } else if (theta > 0) { // SPIN COMMANDS
        sendDriveCommand(POS_THETA_VALUE, theta);
    } else if (theta < 0) {
        sendDriveCommand(NEG_THETA_VALUE, theta);
    }
}

/**
 * Send a command to move the robot at specified speeds
 *
 * @param _x Forward velocity
 * @param _y Lateral velocity
 * @param _theta Rotational velocity
 */
void RovioMotionSystem::setMoveCommand(float _x, float _y, float _theta)
{
    x = _x;
    y = _y;
    theta = _theta;
}

void RovioMotionSystem::setHeadLow(int speed)
{
    sendDriveCommand(HEAD_DOWN_VALUE, speed);
    headPos = LOW_HEAD;
}
void RovioMotionSystem::setHeadMid(int speed)
{
    sendDriveCommand(HEAD_MID_VALUE, speed);
    headPos = MID_HEAD;
}
void RovioMotionSystem::setHeadHigh(int speed)
{
    sendDriveCommand(HEAD_UP_VALUE, speed);
    headPos = HIGH_HEAD;
}

void RovioMotionSystem::raiseHead(int speed)
{
    if (headPos == LOW_HEAD) {
        setHeadMid();
    } else if(headPos == MID_HEAD) {
        setHeadHigh();
    }
}
void RovioMotionSystem::lowerHead(int speed)
{
    if (headPos == HIGH_HEAD) {
        setHeadMid();
    } else {
        setHeadLow();
    }
}

//
// Lower Level Messaging Commands
//

/**
 * Send a parametrized drive command to the Rovio robot
 *
 * @param driveCommand The drive code from WoWee's API
 * @param speed        The drive speed from WoWee's API
 */
void RovioMotionSystem::sendDriveCommand(string driveCommand, int speed)
{
    stringstream s(stringstream::in | stringstream::out);
    s << speed;
    sendMotionCommand(DRIVE_ACTION_VALUE +
                      DRIVE_ACTION_PREFIX + driveCommand +
                      SPEED_ACTION_PREFIX + s.str());
}


/**
 * Send a Rovio "nav&action" command
 *
 * @param command The command grammar to be append to the "nav&action" prefix
 */
void RovioMotionSystem::sendMotionCommand(string command)
{


#ifdef DEBUG_MOTION_COMMANDS
    cout << sendRobotMessage(NAV_ACTION_PREFIX+command);
#else
    sendRobotMessage(NAV_ACTION_PREFIX+command);
#endif
}
