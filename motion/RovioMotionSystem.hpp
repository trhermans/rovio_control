/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @brief Class to deal with underlying motion controls of the Rovio robot.
 *
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 */
#ifndef RovioMotionSystem_hpp_DEFINED
#define RovioMotionSystem_hpp_DEFINED

#include "RovioRobotMessenger.hpp"
#include "MotionSystem.hpp"
#include "TypeDefs.hpp"
#include <string>

#define DEFAULT_SPEED 5

class RovioMotionSystem : public RovioRobotMessenger, public MotionSystem
{
public:
    // Constructors
    RovioMotionSystem(std::string _robotIP);
    virtual ~RovioMotionSystem() {}

    // Drive functions
    virtual void runStep(void);
    virtual void setMoveCommand(float x, float y, float theta);
    void sendDriveCommand(std::string driveCommand, int speed);
    void sendMotionCommand(std::string command);

    // Head functions
    void setHeadLow(int speed = DEFAULT_SPEED);
    void setHeadMid(int speed = DEFAULT_SPEED);
    void setHeadHigh(int speed = DEFAULT_SPEED);
    virtual void raiseHead(int speed = DEFAULT_SPEED);
    virtual void lowerHead(int speed = DEFAULT_SPEED);
    virtual void setHeadVal(HeadPos _pos) { headPos = _pos; }
    virtual const HeadPos getHeadVal() const { return headPos; }

    // Constant Members
    // Strings for use with controlling the robot
    static const std::string NAV_ACTION_PREFIX;
    static const std::string DRIVE_ACTION_PREFIX;
    static const std::string SPEED_ACTION_PREFIX;
    static const std::string DRIVE_ACTION_VALUE;
    static const std::string STOP_VALUE;
    static const std::string POS_X_VALUE;
    static const std::string NEG_X_VALUE;
    static const std::string POS_Y_VALUE;
    static const std::string NEG_Y_VALUE;
    static const std::string POS_THETA_VALUE;
    static const std::string NEG_THETA_VALUE;
    static const std::string POS_X_POS_Y_VALUE;
    static const std::string POS_X_NEG_Y_VALUE;
    static const std::string NEG_X_POS_Y_VALUE;
    static const std::string NEG_X_NEG_Y_VALUE;
    static const std::string HEAD_UP_VALUE;
    static const std::string HEAD_DOWN_VALUE;
    static const std::string HEAD_MID_VALUE;
    static const std::string POS_20_THETA_VALUE;
    static const std::string NEG_20_THETA_VALUE;

private:
    // Members
    HeadPos headPos;
    float x;
    float y;
    float theta;
};
#endif // RovioMotionSystem_hpp_DEFINED
