/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @file   rovio-rc.cpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 * @date   Thu Aug 20 14:06:45 2009
 *
 * @brief  Offline tool for testing affordance learning vision system
 *
 */

// System includes
#include <boost/shared_ptr.hpp>
#include <stdlib.h>
#include <iostream>
#include <sstream>

// Local includes
#include "ImageRetriever.hpp"
#include "OfflineImageRetriever.hpp"
#include "Vision.hpp"

using boost::shared_ptr;
using namespace std;

static shared_ptr<ImageRetriever> retriever;
static shared_ptr<Vision> vision;

// Method Prototypes
// Let's work!
int main(int argc, char ** argv)
{
    string PATH = "images/set1/";

    if (argc > 1) {
        PATH = argv[1];
    }

    // Setup a pointer to the robot motion control
    retriever = shared_ptr<OfflineImageRetriever>(
        new OfflineImageRetriever(PATH));
    vision = shared_ptr<Vision>(new Vision(retriever));

    vision->setOffline();
    while (vision->processImage());

    // Exit
    return EXIT_SUCCESS;
}
