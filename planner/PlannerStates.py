# Copyright (C) 2009 Georgia Institute of Technology
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the GNU Lesser Public License along with Man.  If not, see
# <http://www.gnu.org/licenses/>.


"""
State methods for the RobotPlanner FSA
"""
def doneState(planner):
    if planner.firstFrame():
        # Stop the robot
        planner.motion.setSpeed(0,0,0)
    return planner.stay()

def avoidObstacle(planner):
    if planner.firstFrame():
        planner.motion.setSpeed(-5,0,0)
    if planner.vision.IRObjectDetected:
        return planner.stay()
    else:
        return planner.goLater("doneState")

def turnToObjectHeading(planner):
    if planner.firstFrame():
        # Determine spin direction based on target heading
        spinDir = 1
        planner.motion.setSpeed(0,0, spinDir * 5)

    # Check if target heading is within the correct bounds

    return planner.stay()

def driveToObject(planner):
    if planner.firstFrame():
        planner.motion.setSpeed(1,0,0)

    if planner.vision.IRObjectDetected:
        # We have arrived in front of the object
        return planner.goLater('doneState')

    return planner.stay()
