/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

// Local includes
#include "RobotPlanner.hpp"
#include "TypeDefs.hpp"
#include "ImageRetriever.hpp"
#include "PyMotion.hpp"
#include "PyVision.hpp"

// System includes
#include <Python.h>
#include <string>
#include <iostream>
#include <unistd.h>

using boost::shared_ptr;
using namespace std;

const string PLANNER_MODULE = "planner.RobotPlanner";
const string PLANNER_INSTANCE = "RobotPlanner";

/**
 * Constructs a new RobotPlanner object to control the autonomous agent
 *
 * @param _motion Pointer to the motion controller of the robot
 * @param _vision Pointer to the vision module of the robot
 */
RobotPlanner::RobotPlanner(shared_ptr<MotionController> _motion,
                           shared_ptr<Vision> _vision) :
    PThread("Planner"), motion(_motion), vision(_vision),
    // Python stuff
    planner_module(NULL), planner_instance(NULL)
{
    // Initialize Python
    Py_Initialize();
    set_motion_reference(_motion);
    set_vision_reference(_vision);
    c_init_motion();
    c_init_vision();
    importModules();
    // Get planner instance
    getPlannerInstance();
}

RobotPlanner::~RobotPlanner()
{
    Py_XDECREF(planner_instance);
    Py_XDECREF(planner_module);
}
/**
 * Method to import all required python modules
 */
void RobotPlanner::importModules()
{
    // Add current directory to sys path to allow imports
    PyObject *sys_module = PyImport_ImportModule("sys");
    PyObject *dict = PyModule_GetDict(sys_module);
    PyObject *path = PyDict_GetItemString(dict, "path");
    PyList_Append(path, PyString_FromString("."));
    Py_DECREF(sys_module);

    // Import the python RobotPlanner module
    planner_module = PyImport_ImportModule(PLANNER_MODULE.c_str());

    // Check that the module correctly imported
    if (planner_module == NULL) {
        // error, couldn't import noggin.Brain
        cerr << "Error importing RobotPlanner module" << endl;
        if (PyErr_Occurred()) {
            PyErr_Print();
        } else {
            cerr << "No Python exception information available" << endl;
        }
    }
    Py_XDECREF(planner_instance);
}

void RobotPlanner::getPlannerInstance()
{
    PyObject *dict = PyModule_GetDict(planner_module);
    PyObject *planner_class = PyDict_GetItemString(dict,
                                                   PLANNER_INSTANCE.c_str());

    // Test that the planner class correctly imported
    if (planner_class != NULL) {
        planner_instance = PyObject_CallObject(planner_class, NULL);
    } else {
        planner_instance = NULL;
    }

    // Test that the planner instance worked correctly
    if (planner_instance == NULL) {
        cerr << "Error accessing RobotPlanner.RobotPlanner" << endl;
        if (PyErr_Occurred()) {
            PyErr_Print();
        } else {
            cerr << "No error available" << endl;
        }
    }
}

/**
 * Main control loop for the robot planner.
 */
void RobotPlanner::runStep()
{
    lock();

    // Get Sensor Info
    if (!vision->processImage()) {
        // Failed to get image, we'll shutdown
        cerr << "Failed to process image frame" << endl;
        running = false;
        unlock();
        return;
    }

    // Run the python module
    PyObject *result = PyObject_CallMethod(planner_instance,
                                           (char *)"run", NULL);
    // Ensure that the python module ran smoothly
    if (result == NULL) {
        // report error
        if (PyErr_Occurred()) {
            // test if it is a keyboard interrupt
            if (PyErr_ExceptionMatches(PyExc_KeyboardInterrupt)) {
                // Exit the thread
                cout << endl;
                running = false;
                unlock();
                return;
            }
            PyErr_Print();
        } else {
            cerr << "Error occurred in RobotPlanner.run() method"<< endl;
            cerr << "No Python exception information available" << endl;
        }
    } else {
        Py_DECREF(result);
    }
    unlock();
}

/**
 * Overloaded PThread start function. Starts other threads before
 * starting itself.
 *
 * @return Value of PThread::start()
 */
const bool RobotPlanner::startSubThreads()
{
    // Start the motion thread
    if ( motion->start() != 0) {
        cerr << "Motion thread failed to start" << endl;
        return false;
    }

    // Start the vision retriever
    if (vision->retriever->start() != 0) {
        cerr << "ImageRetriever thread failed to start" << endl;
        return false;
    }

    return true;
}

/**
 * Overloaded PThread run method
 */
void RobotPlanner::run()
{    // Signal thread start
    // Wait for vision thread to get first frame before running
    while(!vision->retriever->isRunning()) {
        //cout << "Waiting for the image retriever" << endl;
    }
    running = true;
    cout << "Running robot planner" << endl;
    // Run the control loop
    while (running) {
        runStep();
    }
}

/**
 * Stop child threads prior to exiting our own thread
 */
void RobotPlanner::stopSubThreads()
{
    // Stop the motion controller
    motion->stop();
    // Stop vision retriever
    vision->retriever->stop();
}
