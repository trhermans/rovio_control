/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @file   RobotPlanner.hpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Main class to coordiante the planning of an autonomous robot system
 *
 */

#ifndef RobotPlanner_hpp_DEFINED
#define RobotPlanner_hpp_DEFINED

// Local libraries
#include "MotionController.hpp"
#include "Vision.hpp"

// System libraries
#include <Python.h>
#include <boost/shared_ptr.hpp>

class RobotPlanner : public PThread
{
public:
    // Constructors
    RobotPlanner(boost::shared_ptr<MotionController> _motion,
                 boost::shared_ptr<Vision> _vision);
    virtual ~RobotPlanner();

    // Main control loop
    void runStep();

    // Overload PThread virtual functions
    virtual const bool startSubThreads();
    virtual void stopSubThreads();
    virtual void run();

private:
    // Private member functions
    void importModules();
    void getPlannerInstance();

    // Private members
    boost::shared_ptr<MotionController> motion;
    boost::shared_ptr<Vision> vision;
    PyObject *planner_module;
    PyObject *planner_instance;
};

#endif // RobotPlanner_hpp_DEFINED
