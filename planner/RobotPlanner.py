# Copyright (C) 2009 Georgia Institute of Technology
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the GNU Lesser Public License along with Man.  If not, see
# <http://www.gnu.org/licenses/>.


import FSA
import PlannerStates
import TrackerStates
from motion import HeadPos
from motion import Motion
from vision import Vision

class RobotPlanner(FSA.FSA):
    """
    Class to handle the main control thread of the robot
    """
    def __init__(self):
        # Initialize our superclass
        FSA.FSA.__init__(self)
        # Add our states to the FSA
        self.addStates(PlannerStates)
        self.addStates(TrackerStates)
        self.setPrintStateChanges(True)
        # set printing to be done with colors
        self.name = "Planner"
        self.stateChangeColor = "red"
        self.currentState = "waitToTrack"
        #self.currentState = "doneState"
        self.motion = Motion()
        self.vision = Vision()

    def run(self):
        """
        Main control loop for the robot behaviors
        """
        self.updateSensors()
        if self.vision.IRObjectDetected:
            self.printf("IR Object detected")

        FSA.FSA.run(self)



    def updateSensors(self):
        if self.vision.headPosition != self.motion.headPosition:
            self.printf("Aligning head position values")
            self.motion.headPosition = self.vision.headPosition
            self.printf("Head position is " + str(self.motion.headPosition))

        elif self.motion.headPosition != HeadPos.LOW_HEAD:
            self.printf("RobotPlanner: Lowering head!")
            self.motion.lowerHead()
