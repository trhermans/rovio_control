# Copyright (C) 2009 Georgia Institute of Technology
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the GNU Lesser Public License along with Man.  If not, see
# <http://www.gnu.org/licenses/>.


DRIVE_TO_TARGET_HEADING = 5
DRIVE_TO_TARGET_DISTANCE = 0
SPIN_TO_TARGET_HEADING = 10
STOP_TARGET_DISTANCE = 5
SPIN_TO_TARGET_SPEED = 5
DRIVE_TO_TARGET_SPEED = 1
