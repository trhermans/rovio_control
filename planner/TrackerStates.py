# Copyright (C) 2009 Georgia Institute of Technology
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the GNU Lesser Public License along with Man.  If not, see
# <http://www.gnu.org/licenses/>.


"""
File defining the states used in tracking an object
"""

import TrackerConstants as Constants
import TrackerTransitions as Transitions
from math import degrees

def waitToTrack(planner):
    if planner.vision.isTracking:
        return startTracking(planner)
    return planner.stay()

def startTracking(planner):
    if Transitions.shouldDriveToTarget(planner):
        return planner.goNow("driveToTarget")
    else:
        return planner.goNow("spinToTarget")

def spinToTarget(planner):
    if planner.firstFrame():
        planner.printf("Target heading is " +
                       str(degrees(planner.vision.targetHeading)))
        planner.printf("Target distance is " + str(planner.vision.targetDistance))
        planner.motion.stopMoving()

    if Transitions.shouldFindLostObject(planner):
        return planner.goLater("findLostObject")

    if Transitions.shouldDriveToTarget(planner):
        return planner.goLater("driveToTarget")

    if planner.vision.targetHeading > 0:
        #planner.motion.spin20DegPos()
        planner.motion.setSpeed(0,0, Constants.SPIN_TO_TARGET_SPEED)
    else:
        #planner.motion.spin20DegNeg()
        planner.motion.setSpeed(0,0, -Constants.SPIN_TO_TARGET_SPEED)

    return planner.stay()

def driveToTarget(planner):
    if planner.firstFrame():
        planner.printf("Target heading is " +
                       str(degrees(planner.vision.targetHeading)))
        planner.printf("Target distance is " + str(planner.vision.targetDistance))
        planner.motion.setSpeed(Constants.DRIVE_TO_TARGET_SPEED, 0, 0)

    if Transitions.shouldSpinToTarget(planner):
        return planner.goLater("spinToTarget")

    if Transitions.shouldFindLostObject(planner):
        return planner.goLater("lostObject")

    if Transitions.shouldStopApproaching(planner):
        return planner.goLater("doneTracking")

    return planner.stay()

def doneTracking(planner):
    if planner.firstFrame():
        planner.motion.stopMoving()
        planner.vision.stopTracking()
        return planner.stay()

    return planner.goLater("waitToTrack")


def lostObject(planner):
    if planner.firstFrame():
        planner.motion.stopMoving()

    if planner.counter > 10:
        return planner.goLater("findLostObject")

    if Transitions.shouldTrackFoundObject(planner):
        return startTracking(planner)

    return planner.stay()

def findLostObject(planner):
    if planner.firstFrame():
        planner.motion.stopMoving()

    if Transitions.shouldTrackFoundObject(planner):
        return startTracking(planner)

    planner.motion.spin20DegPos()

    return planner.stay()
