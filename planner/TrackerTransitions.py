# Copyright (C) 2009 Georgia Institute of Technology
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the GNU Lesser Public License along with Man.  If not, see
# <http://www.gnu.org/licenses/>.

from math import degrees

import TrackerConstants as Constants

def shouldSpinToTarget(planner):
    return (abs(degrees(planner.vision.targetHeading)) > Constants.SPIN_TO_TARGET_HEADING and
            planner.vision.targetDistance < Constants.DRIVE_TO_TARGET_DISTANCE)

def shouldDriveToTarget(planner):
    return (abs(degrees(planner.vision.targetHeading)) <
            Constants.DRIVE_TO_TARGET_HEADING or
            planner.vision.targetDistance > Constants.DRIVE_TO_TARGET_DISTANCE)

def shouldStopApproaching(planner):
    return (planner.vision.IRObjectDetected or
            planner.vision.targetDistance < Constants.STOP_TARGET_DISTANCE)

def shouldFindLostObject(planner):
    return (planner.vision.targetHeading == 0.0)

def shouldTrackFoundObject(planner):
    return planner.vision.targetHeading != 0.0
