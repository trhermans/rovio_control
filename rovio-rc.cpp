/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @file   rovio-rc.cpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 * @date   Thu Aug 20 14:06:45 2009
 *
 * @brief  Interactive remote control for the Rovio robot.
 *
 */

// System includes
#include <boost/shared_ptr.hpp>
#include <stdlib.h>
#include <iostream>
#include <sstream>

// Local includes
#include "MotionController.hpp"
#include "RovioMotionSystem.hpp"
#include "RovioImageRetriever.hpp"
#include "RobotPlanner.hpp"

using boost::shared_ptr;
using namespace std;

static shared_ptr<MotionSystem> ms;
static shared_ptr<MotionController> motion;
static shared_ptr<ImageRetriever> retriever;
static shared_ptr<Vision> vision;
static shared_ptr<RobotPlanner> planner;

// Let's work!
int main(int argc, char ** argv)
{
    // IP address of the robot
    string ipAddress = "192.168.10.18";

    // Setup a pointer to the robot motion control
    ms = shared_ptr<RovioMotionSystem>(new RovioMotionSystem(ipAddress));
    motion = shared_ptr<MotionController>(new MotionController(ms));
    retriever = shared_ptr<RovioImageRetriever>(
        new RovioImageRetriever(ipAddress));
    vision = shared_ptr<Vision>(new Vision(retriever));
    planner = shared_ptr<RobotPlanner>(new RobotPlanner(motion, vision));


    // Running planner as part of this thread
    // First start child threads
    if(!planner->startSubThreads()) {
        return EXIT_FAILURE;
    }
    // Start working
    planner->run();

    // Stop other threads
    planner->stop();

    // Exit
    return EXIT_SUCCESS;
}
