/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef ImageRetriever_hpp_DEFINED
#define ImageRetriever_hpp_DEFINED
/**
 * @file   ImageRetriever.hpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Abstract class defining an interface to the image retriever.
 *
 */
#include "TypeDefs.hpp"
#include "PThread.hpp"
#include <opencv/cv.h>

class ImageRetriever : public PThread
{
public:
    ImageRetriever() : PThread("ImageRetriever") {}
    virtual IplImage getImage() = 0;
    virtual bool grabSensorInfo() = 0;
    virtual const bool isObjectDetected () const = 0;
    virtual const HeadPos getHeadPosition () const = 0;
};
#endif // ImageRetriever_hpp_DEFINED
