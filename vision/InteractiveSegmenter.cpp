/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "InteractiveSegmenter.hpp"
#include <iostream>
#include <cmath>
#include <image.h>
#include <segment-image.h>
#include <converter.hpp>
#include <pnmfile.h>

using namespace std;
//
// Segmenter Class
//
InteractiveSegmenter::InteractiveSegmenter(Vision * _v) :
    Segmenter<SegmentSet, CvPoint>(_v), pIndex(0), haveTarget(false),
    connections(0)
{
    p1.x = 0;
    p1.y = 0;
    p2.x = 0;
    p2.y = 0;
    target.x = 0;
    target.y = 0;

    connections = cvCreateMemStorage( 1024 );
    cvNamedWindow("Segmenter");
    cvSetMouseCallback("Segmenter", onWindowClick, this);
}

InteractiveSegmenter::~InteractiveSegmenter()
{
    cvReleaseMemStorage(&connections );
    cvDestroyWindow("Segmenter");
}

/**
 * Display the image in a window and draw the current bounding box
 *
 * @param img The image to segment
 */
SegmentSet InteractiveSegmenter::segmentImage(IplImage* img)
{
    // Copy the image to have a clean one to display
    SegmentSet roi;
    const int lineWidth = 2;

    // Superpixels, Felzenszwalb
    image<rgb> im = IPLtoFELZS(img);

    float sigma = 0.1;
    float k = 500;
    int min_size = 10;
    int num_ccs;

    image<rgb> *dispIm = segment_image(&im, sigma, k, min_size, &num_ccs);

    roi.img = *dispIm;

    if(vision->tracker->isTracking()) {
        dispImg = FELZStoIPL(dispIm);
    } else {
        dispImg = *cvCloneImage(img);
    }

    // Create a bounding box around our region of interest#
    if (p1.x != p2.x && p1.y != p2.y) {
        // Show a rectangle around the segment
        cvRectangle(&dispImg, p1, p2, cvScalar(  0, 255, 0 ), lineWidth );

        unsigned int minX = min(p1.x, p2.x);
        unsigned int maxX = max(p1.x, p2.x);
        unsigned int minY = min(p1.y, p2.y);
        unsigned int maxY = max(p1.y, p2.y);

        // Check that the bounds are within the image frame
        if (minX < 0) { minX = 0; }
        if (maxX > img->width) { maxX = 0; }
        if (minY < 0) { minY = 0; }
        if (maxY > img->height) { maxY = 0; }

        roi.rect.x = minX;
        roi.rect.y = minY;
        roi.rect.width = maxX - minX;
        roi.rect.height = maxY - minY;

        for (unsigned int y = minY; y < maxY; y++) {
            for (unsigned int x = minX; x < maxX; x++) {
                roi.addToRegion(x,y);
            }
        }
    }

    // Display the image
    cvShowImage("Segmenter", &dispImg);

    // Wait before continuing
    return roi;
}

/**
 * Method to process an OpenCV windowClick
 *
 * @param event The event code
 * @param x X position of the mouse pointer
 * @param y Y position of the mouse pointer
 * @param flags Event modifier flags
 * @param param Pointer to this Segmenter class
 */
void InteractiveSegmenter::onWindowClick(int event, int x, int y, int flags,
                                         void* param)
{
    InteractiveSegmenter * s;
    s = reinterpret_cast<InteractiveSegmenter*>(param);
    Estimate2D est;

    switch(event) {
    case CV_EVENT_LBUTTONDOWN:
        break;
    case CV_EVENT_RBUTTONDOWN:
        break;
    case CV_EVENT_MBUTTONDOWN:
        break;
    case CV_EVENT_LBUTTONUP:
        break;
    case CV_EVENT_RBUTTONUP:
        break;
    case CV_EVENT_MBUTTONUP:
        break;
    case CV_EVENT_RBUTTONDBLCLK:
        s->vision->setInitTrack(true);
        break;
    case CV_EVENT_LBUTTONDBLCLK:
        // Change index point
        if (s->getCurrentIndex()) {
            s->setPoint1(x, y);
        } else {
            s->setPoint2(x, y);
        }
        break;
    case CV_EVENT_MBUTTONDBLCLK:
        break;
    case CV_EVENT_MOUSEMOVE:
        break;
    default:
        break;
    }

}

void InteractiveSegmenter::setSegmentBounds(SegmentSet* seg)
{
    // Update the bounds
    setPoint1(seg->rect.x, seg->rect.y);
    setPoint2(seg->rect.x + seg->rect.width,
              seg->rect.y + seg->rect.height);
    setTargetXY(cvPoint(vision->tracker->getTargetX(),
                        vision->tracker->getTargetY()));

    // Display the image
    const int lineWidth = 2;
    const int targetLength = 5;
    // Show a rectangle around the segment
    cvRectangle(&dispImg, p1, p2, cvScalar(255, 0, 255), lineWidth );

    cvLine(&dispImg,
           cvPoint(target.x - targetLength, target.y - targetLength),
           cvPoint(target.x + targetLength, target.y + targetLength),
           cvScalar (0, 0, 255), lineWidth);
    cvLine(&dispImg,
           cvPoint(target.x - targetLength, target.y + targetLength),
           cvPoint(target.x + targetLength, target.y - targetLength),
           cvScalar (0, 0, 255), lineWidth);

    cvShowImage("Segmenter", &dispImg);
}
