/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef InteractiveSegmenter_hpp_DEFINED
#define InteractiveSegmenter_hpp_DEFINED
#include "Segmenter.hpp"
#include "VisionTypes.hpp"
#include <opencv/cv.h>
/**
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Class to handle image segementation interactively via a GUI
 */
class InteractiveSegmenter : public Segmenter<SegmentSet, CvPoint>
{
public:
    // Constructors
    InteractiveSegmenter(Vision* _v);
    virtual ~InteractiveSegmenter();

    // Member functions
    virtual SegmentSet segmentImage(IplImage* img);
    virtual void setSegmentBounds(SegmentSet* seg);

    //
    // Setters
    //
    void setPoint1(int x, int y) {
        p1.x = x;
        p1.y = y;
    }

    void setPoint2(int x, int y) {
        p2.x = x;
        p2.y = y;
    }

    virtual void setTargetXY(CvPoint p) {
        haveTarget = true;
        target.x = p.x;
        target.y = p.y;
    }

    virtual void clearTarget() {
        haveTarget = false;
        target.x = 0;
        target.y = 0;
    }

    //
    // Getters
    //
    const int getCurrentIndex() {
        pIndex++;
        pIndex = pIndex % 2;
        return pIndex;
    }
    static void onWindowClick(int event, int x, int y, int flags, void* param);

private:
    int pIndex;
    CvPoint p1;
    CvPoint p2;
    CvPoint target;
    bool haveTarget;
    CvMemStorage* connections;
    IplImage  dispImg;
};

#endif // InteractiveSegmenter_hpp_DEFINED
