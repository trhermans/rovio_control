#ifndef ObjectTracker_hpp_DEFINED
#define ObjectTracker_hpp_DEFINED
#include <opencv/cv.h>
#include "TypeDefs.hpp"

template <class SegmentType> class ObjectTracker
{
public:
    // Default Constructor
    ObjectTracker() : targetEst(0.0f, 0.0f), tracking(false) {}
    virtual ~ObjectTracker() {}

    // Base class getters and setters
    virtual const bool isTracking() const {
        return tracking;
    }
    const float getTargetHeading() const {
        return targetEst.bearing;
    }
    const float getTargetDistance() const {
        return targetEst.distance;
    }
    void setTargetHeading(float heading) {
        targetEst.bearing = heading;
    }
    virtual void stopTracking() {
        tracking = false;
    }

    // Pure virtual methods to be declared in the implementing classes
    virtual const int getTargetX() const = 0;
    virtual const int getTargetY() const = 0;
    virtual void initializeObjectTrack(IplImage * img, SegmentType* region) = 0;
    virtual void updateObjectTrack(IplImage * img, SegmentType* region) = 0;

protected:
    Estimate2D targetEst;
    bool tracking;
};


#endif //ObjectTracker_hpp_DEFINED
