/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "OfflineImageRetriever.hpp"
#include <iostream>
#include <sstream>
#include <fstream>
#include <queue>
#include <limits.h>
#include <bitset>
#include <Vision.hpp>

using namespace std;

/**
 * Constructs a OfflineImageRetriever
 *
 * @param ipAddress The ipAddress of the Rovio
 */
OfflineImageRetriever::OfflineImageRetriever(string _path) :
    path(_path), count(0) {}

/**
 * Retrieves an image from the Rovio
 */
IplImage OfflineImageRetriever::getImage()
{
    //fstream inImg;
    stringstream inImgName(ios::in | ios::out);
    inImgName << path << count++ << ".jpg";

    return *cvLoadImage(inImgName.str().c_str());
}

/**
 * Retrieves and decodes the current sensor information from the robot
 *
 * @return False if the sensors fail to be read, true otherwise
 *
 */
bool OfflineImageRetriever::grabSensorInfo()
{
    return true;
}
