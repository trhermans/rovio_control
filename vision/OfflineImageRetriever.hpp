/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @file   OfflineImageRetriever.hpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Class to handle retrieving images from an offline source
 *
 */
#ifndef OfflineImageRetriever_hpp_DEFINED
#define OfflineImageRetriever_hpp_DEFINED

#include "ImageRetriever.hpp"
#include "TypeDefs.hpp"
#include <opencv/cv.h>
#include <string>

class OfflineImageRetriever: public ImageRetriever
{
public:
    OfflineImageRetriever(std::string _path);
    virtual ~OfflineImageRetriever() {}
    virtual IplImage getImage();
    virtual bool grabSensorInfo();

    // PThread stuff
    virtual void run() {}
    virtual void lock() {}
    virtual void unlock() {}


    // Getters
    virtual const HeadPos getHeadPosition () const { return MID_HEAD; }
    virtual const bool isObjectDetected () const { return false; }

private:
    std::string path;
    int count;
};
#endif // OfflineImageRetriever_hpp_DEFINED
