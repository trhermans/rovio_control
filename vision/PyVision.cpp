/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include <boost/python.hpp>
#include <boost/python/object.hpp>
#include <boost/python/module.hpp>
#include <boost/python/args.hpp>
#include <boost/shared_ptr.hpp>

#include "Vision.hpp"
#include "TypeDefs.hpp"

using namespace std;
using namespace boost::python;
using namespace boost;

static shared_ptr<Vision> vision_reference;

/**
 * Class to hold the vision interfacing needed for python
 *
 */
class PyVision {
private:
    shared_ptr<Vision> vision;
public:
    PyVision() {
        vision = vision_reference;
    }
    const int getTargetX() const {
        return vision->tracker->getTargetX();
    }
    const int getTargetY() const {
        return vision->tracker->getTargetY();
    }
    const bool IRObjectDetected() const {
        return vision->retriever->isObjectDetected();
    }
    const HeadPos getSensorHeadPosition() const {
        return vision->retriever->getHeadPosition();
    }

    const float getTargetHeading() const {
        return vision->tracker->getTargetHeading();
    }

    const float getTargetDistance() const {
        return vision->tracker->getTargetDistance();
    }

    const bool isTrackingObject() const {
        return vision->tracker->isTracking();
    }

    void stopTracking() const {
        vision->stopTracking();
    }
};

BOOST_PYTHON_MODULE(_vision)
{
    class_<PyVision>("Vision")
        .add_property("targetX", &PyVision::getTargetX, "Target object's x pos")
        .add_property("targetY", &PyVision::getTargetY, "Target object's y pos")
        .add_property("targetHeading", &PyVision::getTargetHeading,
                      "Heading to the target object")
        .add_property("targetDistance", &PyVision::getTargetDistance,
                      "Distance to target object")
        .add_property("IRObjectDetected", &PyVision::IRObjectDetected,
                      "True if the robot inferred detected an object")
        .add_property("headPosition", &PyVision::getSensorHeadPosition,
                      "Sensor read robot head position")
        .add_property("isTracking", &PyVision::isTrackingObject,
                      "Is the vision system tracking an object")
        .def("stopTracking", &PyVision::stopTracking,
             "Command the vision object to stop tracking")
        ;
};

void c_init_vision() {
    if (!Py_IsInitialized())
        Py_Initialize();
    try {
        init_vision();
    } catch (error_already_set) {
        PyErr_Print();
    }
}

void set_vision_reference(shared_ptr<Vision> _vision)
{
    vision_reference = _vision;
}
