/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef PyVision_hpp_DEFINED
#define PyVision_hpp_DEFINED
#include <boost/shared_ptr.hpp>
void c_init_vision();
// C++ backend insertion (must be set before import)
//    can only be called once (subsequent calls ignored)
void set_vision_reference(boost::shared_ptr<Vision> _motion);
#endif // PyVision_hpp_DEFINED
