/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "RovioImageRetriever.hpp"
#include <iostream>
#include <sstream>
#include <queue>
#include <limits.h>
#include <bitset>
#include <Vision.hpp>
#include <FreeImage.h>
#include <Timer.hpp>
#include "ifdefs.hpp"
using namespace std;
const string RovioImageRetriever::IMAGE_LOCATION = "images/current.jpg";
const string RovioImageRetriever::MOV_LOCATION = "images/mov.mjpg";
const string RovioImageRetriever::GET_IMAGE_COMMAND = "/Jpeg/CamImg1.jpg";
const string RovioImageRetriever::GET_MOVIE_COMMAND =
    "/GetData.cgi?Status=false";
const string RovioImageRetriever::MCU_REPORT_COMMAND =
    "/rev.cgi?Cmd=nav&action=20";
#ifdef RES_320
const string RovioImageRetriever::SET_RES =
    "/ChangeResolution.cgi?ResType=1";
#endif
#ifdef RES_352
const string RovioImageRetriever::SET_RES =
    "/ChangeResolution.cgi?ResType=2";
#endif
#ifdef RES_640
const string RovioImageRetriever::SET_RES =
    "/ChangeResolution.cgi?ResType=3";
#endif
const unsigned int RovioImageRetriever::LOW_HEAD_VAL = 200;
const unsigned int RovioImageRetriever::HIGH_HEAD_VAL = 70;

unsigned int getByte(queue<unsigned char> *data);
unsigned int getDoubleByte(queue<unsigned char> *data);
unsigned int hexToDec(unsigned char x);

/**
 * Constructs a RovioImageRetriever
 *
 * @param ipAddress The ipAddress of the Rovio
 */
RovioImageRetriever::RovioImageRetriever(string ipAddress) :
    RovioRobotMessenger(ipAddress), headlightsOn(false), IRon(false),
    IRdetected(false), count(0)
{}

/**
 * Retrieves an image from the Rovio
 */
IplImage RovioImageRetriever::getImage()
{
    if (count == 0) {
        retrieveImage();
    }
    lock();
    IplImage temp = currentImage;
    unlock();
    return temp;
}

void RovioImageRetriever::retrieveImage() {
    string buffer;
    buffer = sendRobotMessage(GET_IMAGE_COMMAND);

    BYTE* membuff = new BYTE[buffer.size()];
    memcpy(membuff, buffer.c_str(), buffer.size());

    FIMEMORY * hmem = FreeImage_OpenMemory(membuff, buffer.size());

    FIBITMAP* bmp = FreeImage_LoadFromMemory(FIF_JPEG, hmem, 0);

    unsigned int  width = FreeImage_GetWidth(bmp);
    unsigned int  height = FreeImage_GetHeight(bmp);
    unsigned int pitch = FreeImage_GetPitch(bmp);

    IplImage* out_img = cvCreateImage(cvSize(width,height), IPL_DEPTH_8U, 3);

    FREE_IMAGE_TYPE image_type = FreeImage_GetImageType(bmp);

    if (image_type == FIT_RGBF) {
        BYTE* bits = (BYTE*) FreeImage_GetBits(bmp);
        for (unsigned int y = 0; y < height; y++) {
            FIRGBF* pixel = (FIRGBF*)bits;
            for (unsigned int x = 0; x < width ; x++) {
                cvSet2D(out_img, height-y-1,x, CV_RGB(pixel[x].red,
                                                      pixel[x].green,
                                                      pixel[x].blue));
            }
            bits += pitch;
        }
    } else if ((image_type == FIT_BITMAP) && (FreeImage_GetBPP(bmp) == 24)) {
        BYTE* bits = (BYTE*) FreeImage_GetBits(bmp);
        for (unsigned int y = 0; y < height; y++) {
            BYTE* pixel = (BYTE*)bits;
            for (unsigned int x = 0; x < width; x++) {
                cvSet2D(out_img, height-y-1,x, CV_RGB(
                            (double)pixel[FI_RGBA_RED],
                            (double)pixel[FI_RGBA_GREEN],
                            (double)pixel[FI_RGBA_BLUE]));
                pixel += 3;
            }
            bits += pitch;
        }
    }
    free(membuff);
    delete bmp;
    FreeImage_CloseMemory(hmem);

    lock();
    currentImage = *out_img;
    count++;
    unlock();
}

void RovioImageRetriever::run()
{
    // Clock rate for grabbing new images
    const long long FRAME_LENGTH = 300;

    // Set the resolution we want
    sendRobotMessage(SET_RES);
    // Get a first image so that we don't crash when processing the frame
    //retrieveImage();

    // Signal start of running
    running = true;

    while(running) {
        // Record our start time
        const long long startTime = micro_time();

        // Get the new image
        retrieveImage();
        grabSensorInfo();

        const long long sleepTime = FRAME_LENGTH - (micro_time() - startTime);
        // Wait before we run again
        if (sleepTime > 0) {
            usleep(static_cast<useconds_t>(sleepTime));
        }
    }
    running = false;
}


/**
 * Retrieves and decodes the current sensor information from the robot
 *
 * @return False if the sensors fail to be read, true otherwise
 *
 */
bool RovioImageRetriever::grabSensorInfo()
{
    stringstream mcuReport(stringstream::in | stringstream::out);
    // Send the command and record the response
    mcuReport << sendRobotMessage(MCU_REPORT_COMMAND);

    // Get rid of leading nonsense
    mcuReport.seekg(22, ios_base::beg);

    // Read report into a queue
    queue<unsigned char> stuff;
    char c;
    while ((c = mcuReport.get()) != EOF) {
        stuff.push(c);
    }

    // Test if the queue is empty
    if (stuff.size() == 0) {
        return false;
    }
    // Packet Length
    getByte(&stuff);

    //unused
    getByte(&stuff);

    // Wheel encodings, dir, ticks
    // Left
    getByte(&stuff);
    getDoubleByte(&stuff);
    // Right
    getByte(&stuff);
    getDoubleByte(&stuff);
    // Rear
    getByte(&stuff);
    getDoubleByte(&stuff);

    // Unused
    getByte(&stuff);

    // Head position
    unsigned int headVal = getByte(&stuff);
    //cout << endl << "head val is " << headVal << endl;
    if (headVal <= HIGH_HEAD_VAL) {
        headPos = HIGH_HEAD;
    }
    if (headVal >= LOW_HEAD_VAL) {
        headPos = LOW_HEAD;
    }
    if (headVal < LOW_HEAD_VAL &&
        headVal > HIGH_HEAD_VAL) {
        headPos = MID_HEAD;
    }

    // Battery info
    getByte(&stuff);

    // LED / IR / Charger status report
    unsigned char ircharplus;
    ircharplus = stuff.front();
    ircharplus = (unsigned char) getByte(&stuff);

    stringstream irstream(ios::in | ios::out);
    irstream << bitset<CHAR_BIT>( ircharplus );
    // Light LED
    headlightsOn = (irstream.str()[7] == '1');
    // IR power
    IRon = (irstream.str()[6] == '1');
    // IR status
    IRdetected = (irstream.str()[5] == '1');
    // 4-2 charger status, 1,0 unused => ignore
    return true;
}

/**
 * Converts the leading elements from a char queue to an int
 *
 * @param data A queue containing the unsigned chars representing the bytes
 *
 * @return Value of the leading byte
 */
unsigned int getByte(queue<unsigned char> *data)
{
    unsigned int i, j;
    i = hexToDec(data->front());
    data->pop();
    j = hexToDec(data->front());
    data->pop();
    return (i << 4) + j;
}

/**
 * Converts the leading two bytes of a char queue to an int
 *
 * @param data A queue containing the unsigned chars representing the bytes
 *
 * @return Value of the leading double byte
 */
unsigned int getDoubleByte(queue<unsigned char> *data)
{
    unsigned int i, j;
    i = getByte(data);
    j = getByte(data);
    return (i << 8) + j;
}

/**
 * Converts a char value to hexadecimal
 *
 * @param x The char hex value
 *
 * @return The decimal equivalent
 */
unsigned int hexToDec(unsigned char x)
{
    if (x >= '0' && x <= '9') {
        return (unsigned int)( x - '0');
    } else if (x >= 'A' && x <= 'F') {
        return (unsigned int) (x - 'A' + 10);
    } else {
        return 0;
    }
}
