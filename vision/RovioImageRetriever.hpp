/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

/**
 * @file   RovioImageRetriever.hpp
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Class to handle retrieving images from the Rovio robot
 *
 */
#ifndef RovioImageRetriever_hpp_DEFINED
#define RovioImageRetriever_hpp_DEFINED

#include "RovioRobotMessenger.hpp"
#include "ImageRetriever.hpp"
#include "TypeDefs.hpp"
#include "PThread.hpp"
#include <opencv/cv.h>
#include <string>

class RovioImageRetriever: public RovioRobotMessenger, public ImageRetriever
{
public:
    // Constructors
    RovioImageRetriever(std::string ipAddress);
    virtual ~RovioImageRetriever() {}

    // ImageRetriever virtual methods
    virtual IplImage getImage();
    virtual bool grabSensorInfo();

    // PThread virtual methods
    virtual void run();

    // Getters
    virtual const HeadPos getHeadPosition () const { return headPos; }
    const bool areHeadlightsOn () const { return headlightsOn; }
    const bool isIROn () const { return IRon; }
    virtual const bool isObjectDetected () const { return IRdetected; }

    // Constant members
    static const std::string IMAGE_LOCATION;
    static const std::string MOV_LOCATION;
    static const std::string GET_IMAGE_COMMAND;
    static const std::string GET_MOVIE_COMMAND;
    static const std::string MCU_REPORT_COMMAND;
    static const std::string SET_RES;
    static const unsigned int LOW_HEAD_VAL;
    static const unsigned int HIGH_HEAD_VAL;

private:
    // Workhorse methods
    void retrieveImage();

    // Members
    HeadPos headPos;
    bool headlightsOn;
    bool IRon;
    bool IRdetected;
    unsigned int count;
    IplImage currentImage;
};
#endif // RovioImageRetriever_hpp_DEFINED
