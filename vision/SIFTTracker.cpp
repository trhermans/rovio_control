/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "SIFTTracker.hpp"
#include <iostream>
#include <float.h>
#include <limits.h>
using namespace std;

#define DRAW_SIFT_FEATURES
#define FRAME_FRACTION 2

SIFTTracker::SIFTTracker(Vision * _vision) :
    ObjectTracker<SegmentSet>(), vision(_vision), displayingImage(false)
{
}

SIFTTracker::~SIFTTracker()
{
    if (displayingImage) {
        cvDestroyWindow("Keypoints");
    }
}

void SIFTTracker::initializeObjectTrack(IplImage * img, SegmentSet* segment)
{
    SIFTSet curSet = feat(img);
    objectFeatures.clear();
    backgroundFeatures.clear();
    tracking = true;

    unsigned int segMinX = segment->rect.x;
    unsigned int segMaxX = segment->rect.x + segment->rect.width;
    unsigned int segMinY = segment->rect.y;
    unsigned int segMaxY = segment->rect.y + segment->rect.height;
    unsigned int bgXFrame = segment->rect.width / FRAME_FRACTION;
    unsigned int bgYFrame = segment->rect.height / FRAME_FRACTION;

    // Parse out those keypoints associated with our object segment
    for(unsigned int i = 0; i < curSet.size(); ++i) {
        // Check if the keypoint is in one of the specified superpixels
        SIFTKeypoint k = curSet.getKeypoint(i);

        if ((k.key.x > segMinX && k.key.x < segMaxX) &&
            (k.key.y > segMinY && k.key.y < segMaxY)) {
            // Add the keypoint to the tracker
            objectFeatures.addKeypoint(k);
            continue;
        } else if ((k.key.x + bgXFrame > segMinX &&
                    k.key.x < segMaxX + bgXFrame) &&
                   (k.key.y + bgYFrame > segMinY &&
                    k.key.y < segMaxY + bgYFrame)) {
            // Add the keypoint to the background
            backgroundFeatures.addKeypoint(k);
            continue;
        }
    }
    // cout << "Found " << objectFeatures.size() << " object keypoints" << endl;
    // cout << "Found " << backgroundFeatures.size() << " background keypoints"
    //      << endl;
    updateSIFTDisplay(img, &curSet, cvScalar(0,255,0));
    updateSIFTDisplay(img, &backgroundFeatures, cvScalar(0,255,255));
    updateSIFTDisplay(img, &objectFeatures, cvScalar(255,0,255));
    displaySIFTFeatures(img);
    // Determine target heading
    updateTargetEstimate(segment->rect);
}

/**
 * This method calculates the SIFT features for the latest image
 * It then iterates through our set of features to track and matches
 * them against the new set using the Sum of squared distances for the
 * histograms associated with each keypoint. We make sure that the best match
 * is not within a certain threshold of the second best match, so that we do not
 * match to a very poor score.
 *
 * @param img The latest image
 */
void SIFTTracker::updateObjectTrack(IplImage * img, SegmentSet* segment)
{
    unsigned int minX = INT_MAX, maxX = 0, minY = INT_MAX, maxY = 0;
    SIFTSet curSet;
    SIFTSet dispSet;
    SIFTSet bgDispSet;
    float threshold = 5.0;
    const unsigned int NUM_BINS = 128;

    //
    // Only get the SIFT features for a part of the image
    //

    // First build the motion bound for looking at SIFT features
    CvRect roi = segment->rect;
    roi.x = roi.x - roi.width / FRAME_FRACTION;
    roi.y = roi.y - roi.height / FRAME_FRACTION;
    roi.width += 2*(roi.width/FRAME_FRACTION);
    roi.height += 2*(roi.height/FRAME_FRACTION);

    // Ensure that the rectangle is inside of the image
    if (roi.x < 0) { roi.x = 0; }
    if (roi.y < 0) { roi.y = 0; }
    if (roi.width  > img->width - roi.x) { roi.width = img->width - roi.x; }
    if (roi.height > img->height - roi.y) { roi.height = img->height - roi.y; }


    // If we have 0 height or width, check the entire image
    if ( roi.width != 0 && roi.height != 0) {
        //cvSetImageROI(img, roi);
    }

    // Calculate the SIFT points
    curSet = feat(img);

    segment->regions.clear();
    bgSegment.img = segment->img;
    bgSegment.regions.clear();

    // Iterate through the known set of keypoints
    for (unsigned int i = 0; i < objectFeatures.size() ; ++i) {
        float best = FLT_MAX;
        float secondBest = FLT_MAX;
        int bestIndex = -1;

        // Iterate through the new set of keypoints
        for (unsigned int j = 0; j < curSet.size(); ++j) {
            // Determine the sum of squared differences for each point
            float ssd = 0;

            // Calculate the sum of squared differences for the histograms
            for(unsigned int k=0; k < NUM_BINS; ++k) {
                float delta = (objectFeatures.hist(i, 0, k) -
                               curSet.hist(j, 0, k));
                ssd += delta * delta;
            }

            if (ssd < best) {
                secondBest = best;
                best = ssd;
                bestIndex = j;
            } else if (ssd < secondBest) {
                secondBest = ssd;
            }
        }

        // Add match if it is unique
        if(threshold * best <= secondBest && bestIndex != -1) {
            SIFTKeypoint bestKey = curSet.getKeypoint(bestIndex);
            // Save the match to the tracker
            objectFeatures.updateKeypoint(i, bestKey);
            dispSet.addKeypoint(bestKey);

            // Update the segments of the keypoints
            unsigned int curX = curSet.getKeypoint(bestIndex).key.x;
            unsigned int curY = curSet.getKeypoint(bestIndex).key.y;

            segment->addToRegion(curX, curY);
            if( curX < minX) {
                minX = curX;
            }
            if( curX > maxX) {
                maxX = curX;
            }
            if( curY < minY) {
                minY = curY;
            }
            if( curY > maxY) {
                maxY = curY;
            }
        }
    }

    //cout << "Matched " << dispSet.size() << " object keypoints" << endl;

    // Iterate through the known set of background keypoints
    for (unsigned int i = 0; i < backgroundFeatures.size() ; ++i) {
        float best = FLT_MAX;
        float secondBest = FLT_MAX;
        int bestIndex = -1;

        // Iterate through the new set of keypoints
        for (unsigned int j = 0; j < curSet.size(); ++j) {
            // Determine the sum of squared differences for each point
            float ssd = 0;

            // Calculate the sum of squared differences for the histograms
            for(unsigned int k=0; k < NUM_BINS; ++k) {
                float delta = (backgroundFeatures.hist(i, 0, k) -
                               curSet.hist(j, 0, k));
                ssd += delta * delta;
            }

            if (ssd < best) {
                secondBest = best;
                best = ssd;
                bestIndex = j;
            } else if (ssd < secondBest) {
                secondBest = ssd;
            }
        }

        // Add match if it is unique
        if(threshold * best <= secondBest && bestIndex != -1) {
            SIFTKeypoint bestKey = curSet.getKeypoint(bestIndex);
            // Save the match to the tracker
            backgroundFeatures.updateKeypoint(i, bestKey);
            bgDispSet.addKeypoint(bestKey);

            // Update the segments of the keypoints
            unsigned int curX = curSet.getKeypoint(bestIndex).key.x;
            unsigned int curY = curSet.getKeypoint(bestIndex).key.y;
            bgSegment.addToRegion(curX, curY);
            curSet.removeKeypoint(bestIndex);
        }
    }

    //cout << "Matched " << bgDispSet.size() << " background keypoints" << endl;

    // Add additional keypoints to the set
    for(unsigned int t = 0; t < curSet.size(); t++) {
        SIFTKeypoint k = curSet.getKeypoint(t);
        unsigned int curX = k.key.x;
        unsigned int curY = k.key.y;

        // Test if the points are in the current segment
        if (segment->pixelInRegion(curX, curY) &&
            !bgSegment.pixelInRegion(curX, curY)) {
            objectFeatures.addKeypoint(k);
            dispSet.addKeypoint(k);
            if( curX < minX) {
                minX = curX;
            }
            if( curX > maxX) {
                maxX = curX;
            }
            if( curY < minY) {
                minY = curY;
            }
            if( curY > maxY) {
                maxY = curY;
            }
        } else if (!segment->pixelInRegion(curX, curY) &&
                   bgSegment.pixelInRegion(curX, curY)) {
            backgroundFeatures.addKeypoint(k);
            bgDispSet.addKeypoint(k);
        }
    }
    // cout << "Have " << dispSet.size() << " of " << objectFeatures.size()
    //      << " total foreground feature points in this frame." << endl;
    // cout << "Have " << bgDispSet.size() << " of " << backgroundFeatures.size()
    //      << " total background feature points in this frame." << endl;

    updateSIFTDisplay(img, &curSet, cvScalar(0,255,0));
    updateSIFTDisplay(img, &dispSet, cvScalar(255, 0, 255));
    updateSIFTDisplay(img, &bgDispSet, cvScalar(0,255,255));

    // cvRectangle(img, cvPoint(roi.x, roi.y),
    //             cvPoint(roi.x+roi.width, roi.y+roi.width),
    //             cvScalar(255, 0, 0 ));
    displaySIFTFeatures(img);
    segment->updateBounds(minX, minY, maxX, maxY);

    // Determine target heading
    updateTargetEstimate(segment->rect);
}

void SIFTTracker::updateTargetEstimate(CvRect roi)
{
    targetROI.rect = roi;
    targetX = roi.x + roi.width / 2;
    targetY = roi.y + roi.height;
    targetEst = vision->getPixEstimate(targetX, targetY);
}

/**
 * Detect SIFT features
 *
 * @param colorImg Image to detect features on
 *
 * @return Number of features detected
 */
SIFTSet SIFTTracker::feat(IplImage *colorImg)
{
    // Grayscale image
	IplImage *img;

    // SIFT calculation stuff
    SIFTSet curSet;
    VlSiftFilt * filt;
    VlSiftKeypoint const * keypoints;
    int O = -1;
    int S = 3;
    int omin = -1;

    // Get a copy of the image in the correct color space
    CvRect roi = cvGetImageROI(colorImg);
    cvResetImageROI(colorImg);
    img = cvCreateImage(cvGetSize(colorImg), IPL_DEPTH_8U, 1);
    cvCvtColor(colorImg, img, CV_BGR2GRAY);

    // Create a new SIFT filter
    filt = vl_sift_new(roi.width, roi.height, O, S,omin);

    // Set filter parameters
	vl_sift_set_peak_thresh(filt, 1.5);
    vl_sift_set_edge_thresh(filt, 4.0);
    // vl_sift_set_magnif(filt, 2);

    // Convert char data into floats
    vl_sift_pix* data = (vl_sift_pix*)malloc(roi.width*roi.height*
                                             sizeof(vl_sift_pix));
    for (int a = roi.y; a < roi.y + roi.height; ++a) {
        for (int b = roi.x; b < (roi.x + roi.width); ++b) {
            data[(a-roi.y)*roi.width + (b-roi.x)]= (vl_sift_pix)(uint8_t)
                img->imageData[a*img->widthStep + b];
        }
    }
    // cout << "\t\tROI is : (" << roi.x << ", " << roi.y << ")"
    //      << "\t(" << roi.x + roi.width << ", " << roi.y + roi.height << ")"
    //      << endl;

    // Iterate through the SIFT octaves
	for (int err = vl_sift_process_first_octave(filt,
                                                data);
         err!= VL_ERR_EOF;
         err = vl_sift_process_next_octave(filt) ) {

        // Loop scope variables
        unsigned int curNkeys;

        // Run the detector
		vl_sift_detect(filt);

        // Find the keypoints
        keypoints = vl_sift_get_keypoints(filt);
        curNkeys = vl_sift_get_nkeypoints(filt);

        // Iterate through the SIFT keypoints
		for (unsigned int i = 0; i < curNkeys; ++i) {
            // Loop scope variables
            VlSiftKeypoint const *k;
            VlSiftKeypoint ik;

            double angles[4];
            int nangles;
            k = keypoints + i;

            nangles = vl_sift_calc_keypoint_orientations(filt, angles, k);
            vector<vector<int> > hists;
            vector<double > angleI;
            angleI.reserve(128);
            for (int j = 0; j < nangles; ++j) {
                vl_sift_pix descr [128];
                vector<int> descr_i;

                vl_sift_calc_keypoint_descriptor(filt, descr, k, angles[j]);

                for (int l = 0 ; l < 128 ; ++l) {
                    descr_i.push_back((int)(unsigned char) (512.0 * descr [l]));
                }
                hists.push_back(descr_i);
                angleI.push_back(angles[j]);
            }
            vl_sift_keypoint_init(filt, &ik,
                                  keypoints[i].x, keypoints[i].y,
                                  keypoints[i].sigma);
            ik.x += roi.x;
            ik.y += roi.y;
            curSet.addKeypoint(ik, angleI, hists);
		}
	}

    // Remove the sift object
    vl_sift_delete(filt);

    // Cleanup after ourselves
    cvReleaseImage(&img);
	return curSet;
}

void SIFTTracker::updateSIFTDisplay(IplImage* img, SIFTSet* set,
                                    CvScalar circleColor)
{
    for (unsigned int i = 0; i < set->size(); ++i) {
        // Draw circles for the keypoints
        int csize = pow(2.0, set->getKeypoint(i).key.o+1);
        if (csize < 2) {
            csize = 2;
        }

        cvCircle(img, cvPoint((int) set->getKeypoint(i).key.x,
                              (int) set->getKeypoint(i).key.y),
                 csize, circleColor, 1, CV_AA);
        for (unsigned int t = 0;
             t < set->getKeypoint(i).orientations.size();
             ++t) {
            float theta = -set->getAngle(i,t);
            cvLine(img,
                   cvPoint((int) set->getKeypoint(i).key.x,
                           (int) set->getKeypoint(i).key.y),
                   cvPoint((int) (set->getKeypoint(i).key.x + csize*cos(theta)),
                           (int) (set->getKeypoint(i).key.y + csize*sin(theta))),
                   circleColor, 1, CV_AA);
        }
    }
}

void SIFTTracker::displaySIFTFeatures(IplImage* img)
{
    if (!displayingImage) {
        cvNamedWindow("Keypoints");
        displayingImage = true;
    }
    // Draw the SIFT keypoints to the display
    cvShowImage("Keypoints", img);
}

/**
 * Cluster a set of SIFT features
 *
 * @param set The SIFT feature set to cluster
 * @param numClusters Defaults to 3
 */
void SIFTTracker::cluster(SIFTSet* set, int numClusters)
{
    // Create the necessary matrices for clustering
	CvMat* k_mat = cvCreateMat(set->getNFeat(), 128, CV_32FC1);

    // Fill k_mat with histogram data
    for(unsigned int i = 0, x = 0; i < set->size(); ++i, ++x) {
        SIFTKeypoint km = set->getKeypoint(i);
        for(unsigned int j = 0; j < km.orientations.size(); ++j, ++x) {
            for (unsigned int k = 0; k < 128; ++k) {
                k_mat->data.fl[x] = km.orientations[j].hist[k];
            }
        }
    }

	CvMat* l_mat = cvCreateMat(set->getNFeat(), 1, CV_32SC1);
	CvMat* centers = cvCreateMat(numClusters, 128, CV_32FC1);

	cvKMeans2(k_mat, numClusters, l_mat, cvTermCriteria(CV_TERMCRIT_ITER, 200,
                                                        0.1));

    // Calculate the cluster centers
	kmCenter(k_mat, l_mat, centers, numClusters);
    // Normalize the cluster results
    recordDist(set, centers, numClusters);
}

void SIFTTracker::kmCenter(CvMat* samples, CvMat* labels, CvMat* centers,
                           int clusters)
{

	CvMat* sub_cent = cvCreateMat(1, 128, CV_32FC1);
	CvMat* sub_samp = cvCreateMat(1, 128, CV_32FC1);
	cvZero(centers);

	int* c_members = (int*) malloc(clusters*sizeof(int));
	memset(c_members, 0, clusters*sizeof(int));

    CvSize centerSize = cvGetSize(centers);
    CvSize sampleSize = cvGetSize(samples);

	// For each row in the samples matrix, add that row to the row of the centers
	// matrix corresponding to
	// the label at the same row of the labels matrix. Increment the counter for
	// the label that was found
	// and when finished adding samples to the centers matrix, divide each
	// element in each row by the number
	// of samples found for the corresponding label.
	for (int x = 0; x < samples->rows; ++x) {

		int k = (int) round(cvGetReal1D(labels, x));
        if(k < 0 || k > centers->rows) {
            cout << "kmCenter k value of " << k << " is outside bounds of (0,"
                 << centers->rows << ")" << endl;
            free(c_members);
            return;
        }

		cvGetRow(centers, sub_cent, k);
		cvGetRow(samples, sub_samp, x);

		cvAdd(sub_cent, sub_samp, sub_cent);

		++(c_members[k]);

	}

	for (int i = 0; i < centers->rows; ++i) {
		if (c_members[i] > 0) {
			for (int j = 0; j < centers->cols; ++j) {
				centers->data.fl[i*128 + j] /= (float) c_members[i];
                cvSetReal2D(centers, i, j, cvGetReal2D(centers, i, j) /
                            (float) c_members[i]);
            }
        }
    }

	// Update labels in case any changed
	for (int i = 0; i < samples->rows; ++i) {
        vector<int> samps;
        for(unsigned int j = 0; j < 128; j++) {
            samps.push_back((int)samples->data.fl[i*128+j]);
        }
		cvSetReal1D(labels, i, findCentroid(&samps, centers, clusters));
    }

	free(c_members);
}

int SIFTTracker::findCentroid(vector<int>* descr, CvMat* centers, int clusters)
{
	// For each centroid, calculate the (Euclidean) distance between that centroid
	// and the given descriptor.
	// Return the index of the centroid for which the distance is at its minimum.
	float dist = 0;
	float min_dist = FLT_MAX;
	int min_i = -1;

	for (int i = 0; i < clusters; ++i) { // index of centroid
		dist = 0;
		for (int j = 0; j < 128; ++j) { // element of vector
			dist += ((descr->at(j) - centers->data.fl[128*i + j])*
                     (descr->at(j) - centers->data.fl[128*i + j]));
		}

		if (dist < min_dist) {
			min_dist = dist;
			min_i = i;
		}
	}
	return min_i;
}

void SIFTTracker::recordDist(SIFTSet* set, CvMat * centers, int numClusters)
{
	// allocate distribution vector
	float* dist_vector = (float*) calloc(numClusters, sizeof(float));

	// create histogram by projecting words onto clustering
	for (unsigned int i = 0; i < set->size(); ++i) {
        SIFTKeypoint km = set->getKeypoint(i);
        for (unsigned int j = 0; j < km.orientations.size(); j++) {
            int cent = findCentroid(&(km.orientations[j].hist),
                                    centers, numClusters);
            ++(dist_vector[cent]);
        }
	}

    // Normalize histogram and output the results
	for (int i = 0; i < numClusters; ++i) {
		dist_vector[i] /= set->getNFeat();
	}

	free(dist_vector);
}
