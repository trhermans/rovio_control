/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef SIFTTracker_hpp_DEFINED
#define SIFTTracker_hpp_DEFINED
#include "ObjectTracker.hpp"
#include "Vision.hpp"
#include "VisionTypes.hpp"

class SIFTTracker : public ObjectTracker<SegmentSet>
{
public:
    SIFTTracker(Vision * vision);
    virtual ~SIFTTracker();

    // Object Tracker interface methods
    virtual const int getTargetX() const {
        return targetX;
    }
    virtual const int getTargetY() const {
        return targetY;
    }
    virtual void initializeObjectTrack(IplImage * img, SegmentSet* segment);
    virtual void updateObjectTrack(IplImage* img, SegmentSet* segment);

    // SIFT based methods
    SIFTSet feat(IplImage *img);
    void cluster(SIFTSet* set, int numClusters = 3);
    void kmCenter(CvMat* samples, CvMat* labels, CvMat* centers,
                 int numClusters);
    int findCentroid(std::vector<int>* descr, CvMat* centers, int numclusters);
    void recordDist(SIFTSet* set, CvMat* centers, int numClusters);

protected:
    void updateTargetEstimate(CvRect roi);
    void updateSIFTDisplay(IplImage* img, SIFTSet* set, CvScalar circleColor);
    void displaySIFTFeatures(IplImage* img);
    Vision * vision;
    int targetX;
    int targetY;
    SegmentSet targetROI;
    SegmentSet bgSegment;
    SIFTSet objectFeatures;
    SIFTSet backgroundFeatures;
    bool displayingImage;
};

#endif // SIFTTracker_hpp_DEFINED
