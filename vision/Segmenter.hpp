/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef Segmenter_hpp_DEFINED
#define Segmenter_hpp_DEFINED
#include "Vision.hpp"
class Vision;

/**
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Abstract class to handle image segementation
 */
template <class SegmentType, class TargetType> class Segmenter
{
public:
    // Constructors
    Segmenter(Vision* _v) : vision(_v) {}
    virtual ~Segmenter() {}

    // Pure virtual member functions
    virtual SegmentType segmentImage(IplImage * img) = 0;
    virtual void setSegmentBounds(SegmentType* seg) = 0;
    virtual void setTargetXY(TargetType target) = 0;
    virtual void clearTarget() = 0;
protected:
    Vision * vision;
};

#endif // Segmenter_hpp_DEFINED
