/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#include "Vision.hpp"
#include "SIFTTracker.hpp"
#include "MathExtras.hpp"
#include "ifdefs.hpp"
#include <iostream>
#include <sstream>

using boost::shared_ptr;
using namespace boost::numeric;
using namespace std;

//#define LOG_IMAGES

// Camera Parameters
const float Vision::IMAGE_WIDTH_PIX = IMAGE_WIDTH_PIX_DEF;
const float Vision::IMAGE_HEIGHT_PIX = IMAGE_HEIGHT_PIX_DEF;
const float Vision::IMAGE_WIDTH_CM = 0.236f;
const float Vision::IMAGE_HEIGHT_CM = 0.176f;
const float Vision::FOV_X_DEG = 46.4f;
const float Vision::FOV_Y_DEG = 34.8f;
const float Vision::FOV_X_RAD = M_PI * (46.4f / 180.0f);
const float Vision::FOV_Y_RAD = M_PI * (34.8f / 180.0f);
const float Vision::FOCAL_LENGTH_CM = (float)((IMAGE_WIDTH_CM/2.0f) /
                                              tan(FOV_X_RAD/2.0f));
// e.g. 3 mm * mm_to_pix = 176 pixels
const float Vision::CM_TO_PIX_X = IMAGE_WIDTH_PIX/IMAGE_WIDTH_CM;
const float Vision::CM_TO_PIX_Y = IMAGE_HEIGHT_PIX/IMAGE_HEIGHT_CM;
const float Vision::PIX_X_TO_CM = 1.0f / CM_TO_PIX_X;
const float Vision::PIX_Y_TO_CM = 1.0f / CM_TO_PIX_Y;
const float Vision::IMAGE_CENTER_X = (IMAGE_WIDTH_PIX - 1)/2.0f;
const float Vision::IMAGE_CENTER_Y = (IMAGE_HEIGHT_PIX - 1)/2.0f;

//
// Vision Class
//
Vision::Vision(shared_ptr<ImageRetriever> _retriever) :
    retriever(_retriever), frameCounter(0), initializedTrack(false),
    offline(false)
{
    segmenter = shared_ptr<InteractiveSegmenter>(new
                                                 InteractiveSegmenter(this));
    tracker = shared_ptr<SIFTTracker>(new SIFTTracker(this));
}

/**
 * Vision control loop.
 *
 * @return true if the image was correctly processed, false otherwise.
 */
bool Vision::processImage()
{
    IplImage img;

    // Get latest image and sensor info
    img = retriever->getImage();

#ifdef LOG_IMAGES
    stringstream outImgName(ios::out | ios::in);
    outImgName << "images/newSet/" << frameCounter << ".jpg";
    cvSaveImage(outImgName.str().c_str(), &img);
#endif

    SegmentSet roi = segmenter->segmentImage(&img);

    // Track the object
    if ( initializedTrack ) {
        tracker->initializeObjectTrack(&img, &roi);
        initializedTrack = false;
    } else if (tracker->isTracking()) {
        tracker->updateObjectTrack(&img, &roi);
        segmenter->setSegmentBounds(&roi);
    }

    char c;
    if (getOffline()) {
        c = cvWaitKey(333);
    } else {
        c = cvWaitKey(2);
    }

    while (c == 'p') {
        cout << "Paused processing" << endl;
        c = cvWaitKey();
    }

    // Exit on q keystroke
    if (c == 'q' || c == 'Q') {
        exit(0);
    }

    //cout << "Processed frame #" << frameCounter << endl;
    frameCounter++;
    return true;
}

void Vision::stopTracking()
{
    tracker->stopTracking();
    segmenter->clearTarget();
}

/**
 * Determine the distance and bearing to an object in the camera
 *
 * @param pixX The object's x coordinate
 * @param pixY The object's y coordinate
 * @param objectHeight The height of the object above the ground
 *
 * @return An estimated bearing and range to the object
 */
Estimate2D Vision::getPixEstimate(const int pixX, const int pixY,
                                  const float objectHeight)
{
    Estimate2D NULL_ESTIMATE = Estimate2D(0.0f,0.0f);
    if ( pixX >= IMAGE_WIDTH_PIX || pixX < 0  ||
         pixY >= IMAGE_HEIGHT_PIX || pixY < 0  ){
        return NULL_ESTIMATE;
    }
    // declare x,y,z coordinate of pixel in relation to focal point
    ufvector4 pixelInCameraFrame =
        vector4D(FOCAL_LENGTH_CM,
                 ((float)IMAGE_CENTER_X - (float)pixX) * (float)PIX_X_TO_CM,
                 ((float)IMAGE_CENTER_Y - (float)pixY) * (float)PIX_Y_TO_CM);

    // declare x,y,z coordinate of pixel in relation to the ground plane
    ublas::vector <float> pixelInWorldFrame(4);
    ublas::matrix <float, ublas::row_major,
        ublas::bounded_array<float, 16 > > cameraToWorldFrame;
    cameraToWorldFrame = ublas::identity_matrix<float>(4);

    float cameraHeight;
    cameraHeight = 8.89;
    cameraToWorldFrame(2,3) = cameraHeight;


    // transform camera coordinates to world coordinates for a test pixel
    pixelInWorldFrame = prod(cameraToWorldFrame, pixelInCameraFrame);

    // We are going to parameterize the line with one variable t. We find the t
    // for which the line goes through the plane, then evaluate the line at t
    // for the x,y,z coordinate
    float t = 0;

    // calculate t knowing the objectHeight
    if ((cameraToWorldFrame(Z,3) - pixelInWorldFrame(Z)) != 0) {
        t = ( objectHeight - pixelInWorldFrame(Z) ) /
            ( cameraToWorldFrame(Z,3) - pixelInWorldFrame(Z) );
    }

    const float x = pixelInWorldFrame(X) +
        (cameraToWorldFrame(X,3) - pixelInWorldFrame(X))*t;
    const float y = pixelInWorldFrame(Y) +
        (cameraToWorldFrame(Y,3) - pixelInWorldFrame(Y))*t;
    const float z = pixelInWorldFrame(Z) +
        (cameraToWorldFrame(Z,3) - pixelInWorldFrame(Z))*t;

    ublas::vector<float> objectInWorldFrame = vector4D(x,y,z);

    Estimate2D pixEst(0.0f, 0.0f);

    // distance as projected onto XY plane - ie bird's eye view
    pixEst.distance = hypot(objectInWorldFrame(X), objectInWorldFrame(Y));

    // calculate in radians the bearing to the object8
    // since trig functions can't handle 2 Pi, we need to differentiate
    // by quadrant:

    const bool yPos = objectInWorldFrame(Y) >= 0;
    const bool xPos = objectInWorldFrame(X) >= 0;
    const float temp = objectInWorldFrame(Y) / objectInWorldFrame(X);
    if (!isnan(temp)) {
        //quadrants +x,+y and +x-y
        if( xPos && (yPos || !yPos) ) {
            pixEst.bearing = std::atan(temp);
        } else if( yPos) { //quadrant -x+y
            pixEst.bearing = std::atan(temp) + M_PI;
        } else {//quadrant -x+y
            pixEst.bearing = std::atan(temp) - M_PI;
        }
    } else {
        pixEst.bearing = 0.0f;
    }

    pixEst.bearing = subPIAngle(pixEst.bearing);

    return pixEst;
}
