/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef Vision_hpp_DEFINED
#define Vision_hpp_DEFINED

#include <vector>
#include <boost/shared_ptr.hpp>
#include <string>
#include <opencv/cxcore.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cmath>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include "ImageRetriever.hpp"
#include "TypeDefs.hpp"
#include "ObjectTracker.hpp"
#include "Segmenter.hpp"
#include "InteractiveSegmenter.hpp"
#include "VisionTypes.hpp"

template <class SegmentType, class TargetType> class Segmenter;
// We use this to access the x,y,z components of vectors
enum Cardinal {
    X = 0,
    Y = 1,
    Z = 2
};

typedef SegmentSet segment_type;

/**
 * @author Tucker Hermans <thermans@cc.gatech.edu>
 *
 * @brief  Main control class for vision processing
 */
class Vision
{
public:
    // Constructors
    Vision(boost::shared_ptr<ImageRetriever> _retriever);
    virtual ~Vision() {}

    // Member functions
    bool processImage();
    Estimate2D getPixEstimate(const int pixX, const int pixY,
                              const float objectHeight = 0.0f);
    void stopTracking();
    // Getters & Setters
    void setInitTrack(bool _track) {
        initializedTrack = _track;
    }
    const bool getInitTrack() const {
        return initializedTrack;
    }
    void setOffline(bool _offline=true) {
        offline = _offline;
    }
    const bool getOffline() const {
        return offline;
    }
    // Public members
    boost::shared_ptr<ImageRetriever> retriever;
    boost::shared_ptr<Segmenter<segment_type, CvPoint> > segmenter;
    boost::shared_ptr<ObjectTracker<segment_type> > tracker;

    // Constants
    static const float IMAGE_WIDTH_PIX;
    static const float IMAGE_HEIGHT_PIX;
    static const float IMAGE_WIDTH_CM;
    static const float IMAGE_HEIGHT_CM;
    static const float FOV_X_DEG;
    static const float FOV_Y_DEG;
    static const float FOV_X_RAD;
    static const float FOV_Y_RAD;
    static const float FOCAL_LENGTH_CM;
    static const float CM_TO_PIX_X;
    static const float CM_TO_PIX_Y;
    static const float PIX_X_TO_CM;
    static const float PIX_Y_TO_CM;
    static const float IMAGE_CENTER_X;
    static const float IMAGE_CENTER_Y;

private:
    // Types
    typedef boost::numeric::ublas::vector<
        float, boost::numeric::ublas::bounded_array<float,4> > ufvector4;

    // Functions
    const ufvector4 vector4D(const float x, const float y,
                             const float z, const float w = 1.0f) {
        ufvector4 p = boost::numeric::ublas::zero_vector <float> (4);
        p(X) = x;
        p(Y) = y;
        p(Z) = z;
        p(3) = w;
        return p;
    }

    // Members
    boost::numeric::ublas::matrix <float> cameraToWorldFrame;
    unsigned int frameCounter;
    bool initializedTrack;
    bool offline;
};

#endif // Vision_hpp_DEFINED
