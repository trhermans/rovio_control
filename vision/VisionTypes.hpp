/*
  Copyright (C) 2009 Georgia Institute of Technology

  This library is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser Public License for more details.

  You should have received a copy of the GNU General Public License
  and the GNU Lesser Public License along with Man.  If not, see
  <http://www.gnu.org/licenses/>.
*/

#ifndef VisionTypes_hpp_DEFINED
#define VisionTypes_hpp_DEFINED
#include <set>
#include <vector>
#include <opencv/cv.h>
#include <image.h>
#include <misc.h>
extern "C" {
#include <vl/sift.h>
}

//
// Class to hold an image with an associated set of the segments of interest
//
struct SegmentSet
{
    // Constructors & Destructors
    SegmentSet() : regions() {}
    ~SegmentSet() {}

    // Returns the superpixel ID associated with the point at (x,y)
    const unsigned int getPixelID(unsigned int x, unsigned int y) {
        return img.data[y*img.width() + x].idx;
    }

    // Returns true if the superpixel containing (x,y) is part of the region
    const bool pixelInRegion(unsigned int x, unsigned int y) {
        return regions.find(getPixelID(x,y)) != regions.end();
    }

    // Adds the superpixel containing the point (x,y) to the region
    void addToRegion(unsigned int x, unsigned int y) {
        regions.insert(getPixelID(x,y));
    }

    void updateBounds(unsigned int x1, unsigned int y1,
                      unsigned int x2, unsigned int y2) {
        rect.x = x1;
        rect.y = y1;
        rect.width = x2 - x1;
        rect.height = y2 - y1;
    }

    CvRect rect;
    image<rgb> img;
    std::set<unsigned int> regions;
};

//
// Classes to hold the SIFT feature data
//
struct SIFTOrientation
{
    SIFTOrientation() {
        hist.reserve(128);
    }
    SIFTOrientation(double _angle, std::vector<int> _hist):
        angle(_angle), hist(_hist) {}
    double angle;
    std::vector<int> hist;
};

struct SIFTKeypoint
{
    SIFTKeypoint() {}
    SIFTKeypoint(VlSiftKeypoint _key, std::vector<double> _angles,
                 std::vector<std::vector<int > > _hists) : key(_key) {

        for(unsigned int i = 0; i < _angles.size(); i++) {
            orientations.push_back(SIFTOrientation(_angles[i], _hists[i]));
        }
    }
    VlSiftKeypoint key;
    std::vector<SIFTOrientation> orientations;
};

class SIFTSet
{
public:
    SIFTSet() {}
    void addKeypoint(VlSiftKeypoint _key, std::vector<double>_angles,
                     std::vector<std::vector<int > > _hists) {
        keypoints.push_back(SIFTKeypoint(_key, _angles, _hists));
        nfeat += _angles.size();
    }

    void addKeypoint(SIFTKeypoint k) {
        keypoints.push_back(k);
        nfeat += k.orientations.size();
    }
    int hist(unsigned int i, unsigned int j, unsigned int k) {
        return keypoints[i].orientations[j].hist[k];
    }
    unsigned int size() {
        return keypoints.size();
    }
    SIFTKeypoint getKeypoint(unsigned int i) {
        return keypoints[i];
    }

    void removeKeypoint(unsigned int i) {
        keypoints.erase(keypoints.begin()+i);
    }

    void updateKeypoint(unsigned int i, SIFTKeypoint k) {
        keypoints[i] = k;
    }

    double getAngle(unsigned int i, unsigned int j) {
        return keypoints[i].orientations[j].angle;
    }

    const unsigned int getNFeat() const {
        return nfeat;
    }

    void clear() {
        keypoints.clear();
        nfeat = 0;
    }
private:
    std::vector<SIFTKeypoint> keypoints;
    unsigned int nfeat;
};
#endif // VisionTypes_hpp_DEFINED
